#ifndef LOCALSTORAGE_H
#define LOCALSTORAGE_H

/**
 * An interface for working with data stored
 * locally on the device.
 * @author Adam Knox
 */

#include <MAUtil/Vector.h>
#include <maapi.h>

#include "Settings.h"
#include "Enum.h"
#include "Password.h"
#include "Model.h"

class Model;

class LocalStorage {
public:
	/**
	 * Constructor
	 * @author Adam Knox
	 */
	LocalStorage();

	/**
	 * Destructor
	 * @author Adam Knox
	 */
	virtual ~LocalStorage();

	/**
	 * Saves the existing model to local storage
	 * @author Adam Knox
	 * @param model the model to save
	 * @return true if successful, false if failed
	 */
	static bool savePasswordList(const MAUtil::Vector<Password> passwordList);

	static MAUtil::Vector<Password> LocalStorage::readPasswordList();

	static bool LocalStorage::saveLicense(const MAUtil::String newLicense);

	static MAUtil::String LocalStorage::readLicense();


private:
};

#endif
