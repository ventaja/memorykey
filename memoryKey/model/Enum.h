#ifndef ENUM_H
#define ENUM_H

	// The list of windows available to display
	enum Windows {HOME, CREATE_PASSWORD, VIEW_KEY, EDIT_PASSWORD, RENEW_LICENSE, INPUT_PICTURE_GRID};

	// The possible windows used to input a sequence into with a number assigned for storage
	enum InputMethod {PICTURE_GRID = 0};

#endif
