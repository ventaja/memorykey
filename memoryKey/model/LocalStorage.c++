/**
 * An interface for working with data stored
 * locally on the device.
 * @author Adam Knox
 */

#include "LocalStorage.h"
//#include <conprint.h>

#define PASSWORD_STORE "MemoryKey_passwords"
#define SINGLE_PASSWORD_STORE "MemoryKey_password"
#define LICENSE_STORE "MemoryKey_license"


/**
 * Constructor
 * @author Adam Knox
 */
LocalStorage::LocalStorage() {
}





/**
 * Destructor
 * @author Adam Knox
 */
LocalStorage::~LocalStorage() {}





/**
 * Saves the existing model to local storage
 * @author Adam Knox
 * @param model the model to save
 * @return true if successful, false if failed
 */
bool LocalStorage::savePasswordList(const MAUtil::Vector<Password> passwordList) {
	int startIndex = 0;
	// Open the store
	MAHandle store;
	if (Settings::MULTIPLE_PASSWORDS) {
		store = maOpenStore(PASSWORD_STORE, MAS_CREATE_IF_NECESSARY);
	} else {
		store = maOpenStore(SINGLE_PASSWORD_STORE, MAS_CREATE_IF_NECESSARY);
	}

	// Figure out how much storage space is needed
	int numPasswords = passwordList.size();
	int spaceRequired = sizeof(int) + numPasswords * sizeof(Password);

	// Create data object
	MAHandle passwordData = maCreatePlaceholder();
	if(maCreateData(passwordData, spaceRequired) == RES_OK) {
		// Store the number of passwords being stored
		maWriteData(passwordData, &numPasswords, startIndex, sizeof(int));
		startIndex = sizeof(int);

		// Store each vector password sequentially in the store after the number of passwords
		for (int i = 0; i < numPasswords; i++) {
			maWriteData(passwordData, &passwordList[i], startIndex, sizeof(Password));
			startIndex += sizeof(Password);
		}
	}

	// Write the data object to persistent storage
	int result = maWriteStore(store, passwordData);
	maDestroyObject(passwordData);

	switch(result) {
		case STERR_FULL:
			//todo: Failed, not enough space to save the data.
			break;

		case STERR_NONEXISTENT:
			//todo: Failed, the store doesn't exist!
			break;

		case STERR_GENERIC:
			//todo: Unknown error, possibly a device fault.
			break;

		default:
			//todo: Close the store. 0 means keep it, any other value will cause the store to be deleted
			maCloseStore(store, 0);
			return true;
			break;
	}

	maCloseStore(store, 0);

	return true;
}





/**
 * Gets the list of passwords that is stored locally
 * @author Adam Knox
 * @return The model
 */
MAUtil::Vector<Password> LocalStorage::readPasswordList() {
	Vector<Password> passwordList;
	int numPasswords;
	int startIndex = 0;
	MAHandle passwordData = maCreatePlaceholder();
	MAHandle store;
	if (Settings::MULTIPLE_PASSWORDS) {
		store = maOpenStore(PASSWORD_STORE, 0);
	} else {
		store = maOpenStore(SINGLE_PASSWORD_STORE, 0);
	}

	// The store exists, so we can read from it.
	if(store != STERR_NONEXISTENT) {
		int result = maReadStore(store, passwordData);
		if(result == RES_OUT_OF_MEMORY) {
			//todo: out of memory error
		}

		// Get the number of passwords to read into the vector
		maReadData(passwordData, &numPasswords, startIndex, sizeof(int));

		// Read in the passwords
		startIndex = sizeof(int);
		for (int i = 0; i < numPasswords; i++) {
			Password password;
			maReadData(passwordData, &password, startIndex, sizeof(Password));
			passwordList.add(password);
			startIndex += sizeof(Password);
		}

		maDestroyObject(passwordData);

		return passwordList;
	}

	// Store does not exist, so create a starter list
	else {
		Vector<Password> newPasswordList;

		// Make sure a password is created if in single password mode since the user can't create it
		if (!Settings::MULTIPLE_PASSWORDS) {
			newPasswordList.add(*(new Password(	Settings::SINGLE_PASSWORD_NAME(),
												Settings::SINGLE_PASSWORD_DESC(),
												Settings::SINGLE_PASSWORD_INPUT,
												Settings::SINGLE_PASSWORD_IDALGORITHM,
												Settings::SINGLE_PASSWORD_SPECIALCHARS,
												Settings::SINGLE_PASSWORD_NUMBERS,
												Settings::SINGLE_PASSWORD_UPPER_CASE,
												Settings::SINGLE_PASSWORD_LOWER_CASE,
												Settings::SINGLE_PASSWORD_MAX_LENGTH)));
		}

		return newPasswordList;
	}
}





bool LocalStorage::saveLicense(const MAUtil::String data) {
	// Open the store
	MAHandle store = maOpenStore(LICENSE_STORE, MAS_CREATE_IF_NECESSARY);

	// Create data object
	MAHandle storeData = maCreatePlaceholder();
	if(maCreateData(storeData, sizeof(data)) == RES_OK) {
		// Store the data
		maWriteData(storeData, &data, 0, sizeof(data));
	}

	// Write the data object to persistent storage
	int result = maWriteStore(store, storeData);
	maDestroyObject(storeData);

	switch(result) {
		case STERR_FULL:
			//todo: Failed, not enough space to save the data.
			break;

		case STERR_NONEXISTENT:
			//todo: Failed, the store doesn't exist!
			break;

		case STERR_GENERIC:
			//todo: Unknown error, possibly a device fault.
			break;

		default:
			//todo: Close the store. 0 means keep it, any other value will cause the store to be deleted
			maCloseStore(store, 0);
			return true;
			break;
	}

	maCloseStore(store, 0);
	return false;
}





MAUtil::String LocalStorage::readLicense() {
	MAHandle storeData = maCreatePlaceholder();
	MAHandle store = maOpenStore(LICENSE_STORE, 0);

	// The store exists, so we can read from it.
	if(store != STERR_NONEXISTENT) {
		MAUtil::String data;
		int result = maReadStore(store, storeData);
		if(result == RES_OUT_OF_MEMORY) {
			//todo: out of memory error
		}

		// Get the data
		//printf("%d",maGetDataSize(storeData));
		maReadData(storeData, &data, 0, maGetDataSize(storeData));

		// GEt rid of the store data temp. object
		maDestroyObject(storeData);
		return "Alpha";//data;
	}

	// Store does not exist, so return default
	else {
		return "OOPS, NO LICENSE";
	}

}
