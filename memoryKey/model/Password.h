#ifndef PASSWORD_H
#define PASSWORD_H

#include <MAUtil\String.h>

#include "Enum.h"
#include "Settings.h"

class Password {
public:
	Password(const char t_name[TITLE_LENGTH],
			const char t_description[DESC_LENGTH],
			const InputMethod t_idInputMethod,
			const int t_idAlgorithm,
			const int t_numRequiredSpecialCharacters,
			const int t_numRequiredNumbers,
			const int t_numRequirdUpperCase,
			const int t_numRequiredLowerCase,
			const int t_maxPasswordLength);

	Password();
	virtual ~Password();
	MAUtil::String getName();
	int getIdInputMethod();
	void clone(const Password password);

	char name[TITLE_LENGTH];		//The name that appears in the password list
	char description[DESC_LENGTH];	//A description or hint for this password
	InputMethod idInputMethod;	//The ID number of the password generator algorithm being used
	int idAlgorithm;			//The ID number of the password generator algorithm being used

	//Fields dictating how the password must be formatted
	int numRequiredSpecialCharacters;
	int numRequiredNumbers;
	int numRequiredUpperCase;
	int numRequiredLowerCase;
	int maxPasswordLength;


protected:


private:
};

#endif
