#include "Password.h"

/**
 * Constructor. Creates a new password object and sets all the parameters
 */
Password::Password(const char t_name[TITLE_LENGTH], const char t_description[DESC_LENGTH], const InputMethod t_idInputMethod, const int t_idAlgorithm, const int t_numRequiredSpecialCharacters, const int t_numRequiredNumbers, const int t_numRequiredUpperCase, const int t_numRequiredLowerCase, const int t_maxPasswordLength) {
	for (int i = 0; i < TITLE_LENGTH - 1; i++) {
		name[i] = t_name[i];
	}
	name[TITLE_LENGTH - 1] = '\0';
	for (int i = 0; i < DESC_LENGTH - 1; i++) {
		description[i] = t_description[i];
	}
	description[DESC_LENGTH - 1] = '\0';

	idInputMethod = t_idInputMethod;
	idAlgorithm = t_idAlgorithm;
	numRequiredSpecialCharacters = t_numRequiredSpecialCharacters;
	numRequiredNumbers = t_numRequiredNumbers;
	numRequiredUpperCase = t_numRequiredUpperCase;
	numRequiredLowerCase = t_numRequiredLowerCase;
	maxPasswordLength = t_maxPasswordLength;
}

Password::Password() {}

/**
 * Destructor. Destroys the password object and its sub objects
 */
Password::~Password () {

}

MAUtil::String Password::getName() {
	return name;
}

int Password::getIdInputMethod () {
	return idInputMethod;
}

void Password::clone(const Password password) {
	for (int i = 0; i < TITLE_LENGTH - 1; i++) {
		name[i] = password.name[i];
	}
	name[TITLE_LENGTH - 1] = '\0';
	for (int i = 0; i < DESC_LENGTH - 1; i++) {
		description[i] = password.description[i];
	}
	description[DESC_LENGTH - 1] = '\0';

	idInputMethod = password.idInputMethod;
	idAlgorithm = password.idAlgorithm;
	numRequiredSpecialCharacters = password.numRequiredSpecialCharacters;
	numRequiredNumbers = password.numRequiredNumbers;
	numRequiredUpperCase = password.numRequiredUpperCase;
	numRequiredLowerCase = password.numRequiredLowerCase;
	maxPasswordLength = password.maxPasswordLength;
}
