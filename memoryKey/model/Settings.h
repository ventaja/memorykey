#ifndef SETTINGS_H
#define SETTINGS_H

#include <MAUtil/String.h>
#include "Enum.h"

#define TITLE_LENGTH 20
#define DESC_LENGTH 256

class Settings {
public:
	// The minimum number of icons that must be pressed
	static const int MIN_ICON_SEQUENCE_LENGTH	= 4;

	// The maximum number of characters allowed in a key
	static const int MAX_NUMBERS 		= 30;
	static const int MAX_SPECIAL_CHARS 	= 30;
	static const int MAX_UPPER_CASE 	= 30;
	static const int MAX_LOWER_CASE 	= 30;
	static const int MAX_LENGTH 		= 30;

	// Enable/Disable having more than one password
	static const bool MULTIPLE_PASSWORDS	= true;

	// The settings for the password if only one is allowed
	static const char* SINGLE_PASSWORD_NAME() 		{return "Quintesec";};
	static const char* SINGLE_PASSWORD_DESC() 		{return "";};
	static const InputMethod SINGLE_PASSWORD_INPUT 	= PICTURE_GRID;
	static const int SINGLE_PASSWORD_IDALGORITHM 	= 0;
	static const int SINGLE_PASSWORD_SPECIALCHARS	= 0;
	static const int SINGLE_PASSWORD_NUMBERS		= 3;
	static const int SINGLE_PASSWORD_UPPER_CASE		= 3;
	static const int SINGLE_PASSWORD_LOWER_CASE		= 2;
	static const int SINGLE_PASSWORD_MAX_LENGTH		= 10;

	// The date program testing will end
	static const int  TESTING_YEAR						= 2012;		// Then year in which testing will be done
	static const int  TESTING_MONTH						= 10;		// The last month of said year that testing will be done


private:
	Settings ();
	virtual ~Settings ();
};


#endif
