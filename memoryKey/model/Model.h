#ifndef MODEL_H
#define MODEL_H


#include <MAUtil/String.h>
#include <MAUtil/Vector.h>


#include "Enum.h"
#include "Password.h"
#include "Settings.h"
#include "LocalStorage.h"
#include "../Controller.h"
#include "../generators/Generator.h"
#include "../ui/Input_PictureGrid.h"
#include "../ui/ViewKey.h"
#include "../ui/Menu.h"
#include "../ui/CreatePassword.h"
#include "../ui/EditPassword.h"
#include "../ui/RenewLicense.h"

#define MODEL Model::getInstance()

class Menu;
class CreatePassword;
class EditPassword;
class ViewKey;
class Input_PictureGrid;
class RenewLicense;

class Model {
public:
	// Functions
	~Model ();

	static 	Model* getInstance();

	MAUtil::Vector<Password> getPasswords();
	void addPassword(const Password password);
	void editPassword(const Password oldPassword, const Password newPassword);
	void removePassword(const Password password);
	MAUtil::String getLicense();
	void setLicense(MAUtil::String code);

	// Main Screens
	Menu* 				menuScreen;
	CreatePassword*	 	createPasswordScreen;
	EditPassword*	 	editPasswordScreen;
	RenewLicense*		renewLicenseScreen;
	ViewKey* 			viewKeyScreen;

	// Input Screens
	Input_PictureGrid* inputScreen_PictureGrid;

private:

	Model ();
	Model( const Model& );
	Model& operator=( const Model& );

	static void init();

	static 	Model* 				instance;		// Manages the stored data

	MAUtil::Vector<Password> 	passwordList;
	MAUtil::String	 			license;
};

#endif
