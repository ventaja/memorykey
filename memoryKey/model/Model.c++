#include "Model.h"

Model* Model::instance = NULL;

Model::Model () {}





Model::~Model () {}




void Model::init () {
	Controller ctrl;

	// Create the model
	instance = new Model ();

	// Set the license
	MODEL->license = LocalStorage::readLicense();

	// Create the password list
	MODEL->passwordList = LocalStorage::readPasswordList();//new MAUtil::List<Password*> ();

	// Create universal screens
	MODEL->renewLicenseScreen = new RenewLicense();
	MODEL->inputScreen_PictureGrid = new Input_PictureGrid();
	MODEL->viewKeyScreen = new ViewKey();

	// Multiple Password Mode
	if (Settings::MULTIPLE_PASSWORDS) {
		// Create the user interface screens
		MODEL->menuScreen = new Menu();
		MODEL->createPasswordScreen = new CreatePassword();
		MODEL->editPasswordScreen = new EditPassword();
	}

	ctrl.showScreen(HOME);

}

Model* Model::getInstance () {
	if (instance == NULL) {
		init();
	}
	return instance;
}





MAUtil::Vector<Password> Model::getPasswords() {
	return passwordList;
}





void Model::addPassword(const Password password) {
	//add password to data structure
	passwordList.add(password);

	// Store the new model in persistent storage
	LocalStorage::savePasswordList(passwordList);
}


void Model::editPassword(const Password oldPassword, const Password newPassword) {
	for (int i = 0; i < passwordList.size(); i++) {
		if (strcmp(passwordList[i].name, oldPassword.name) == 0) {
			passwordList.remove(i);
			passwordList.add(newPassword);
		}
	}

	// Store the new model in persistent storage
	LocalStorage::savePasswordList(passwordList);
}


void Model::removePassword(const Password password) {
	for (int i = 0; i < passwordList.size(); i++) {
		if (strcmp(passwordList[i].name, password.name) == 0) {
			passwordList.remove(i);
		}
	}

	// Store the new model in persistent storage
	LocalStorage::savePasswordList(passwordList);
}




MAUtil::String Model::getLicense() {
	return license;
}

void Model::setLicense(MAUtil::String code) {
	MODEL->license = code;
	LocalStorage::saveLicense(code);
}
