#include <MAUtil/Moblet.h>
#include <MAUtil/String.h>
#include <conprint.h>

#include "model/Model.h"

class Init :
	public Moblet
{

public:
	Init() {
		model = MODEL;
	}

	~Init() {
		delete model;
	}

	// Kill the program if it loses focus
	void keyPressEvent(int keyCode, int nativeCode) {
		if(keyCode == EVENT_TYPE_FOCUS_LOST ) {
			maExit(0);
		}
	}

private:
	Model* model;

};





/**
 * description: Program Entry function
 * @author Adam Knox
 * @return success/fail
 **/
extern "C" int MAMain() {
	MAUtil::Moblet::run(new Init());
	return 0;
}
