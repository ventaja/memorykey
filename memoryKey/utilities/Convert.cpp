/*
 * Convert.cpp
 *
 *  Created on: 24 May 2010
 *      Author: orac7
 */
#include "Convert.h"
#include <mavsprintf.h>
#include "snprintf.h"
#include <mawstring.h>
#include <MAUtil/String.h>
#include <wchar.h>

Convert::Convert()
{}

Convert::~Convert()
{}

int Convert::toInt(const char* digit)
{
   int sign = 1;
   int result = 0;

   // check sign
   if (*digit == '-')
   {
	  sign = -1;
	  digit++;
   }

   //--- get integer portion
   while (*digit >= '0' && *digit <='9')
   {
	  result = (result * 10) + *digit-'0';
	  digit++;
   }

   //--- set to sign given at the front
   return result * sign;
}

int Convert::toInt(const MAUtil::BasicString<char>& input)
{
	return toInt(input.c_str());
}

int Convert::toInt(String& input)
{
	return toInt(input.c_str());
}

double Convert::toDouble(String& input)
{
	return toDouble(input.c_str());
}

double Convert::toDouble(const char* input)
{
   int sign = 1;
   double result = 0;
   const char* digit = input;

   // check sign
   if (*digit == '-')
   {
	  sign = -1;
	  digit++;
   }

   //--- get integer portion
   while (*digit >= '0' && *digit <='9')
   {
	  result = (result * 10) + *digit-'0';
	  digit++;
   }

   //--- get decimal point and fraction, if any.
   if (*digit == '.')
   {
	  digit++;
	  double scale = 0.1;
	  while (*digit >= '0' && *digit <='9') {
		 result += (*digit-'0') * scale;
		 scale *= 0.1;
		 digit++;
	  }
   }

   //--- error if we're not at the end of the number
   if (*digit != 0) {
	  return 0.00;
   }

   //--- set to sign given at the front
   return result * sign;
}

time_t Convert::toDateTime(String& input)
{
	return (time_t)toInt(input);
}

String Convert::toString(bool input)
{
	if(input)
		return "1";
	else
		return "0";
}

String Convert::toString(int input)
{
	char buffer[128];
	snprintf(buffer, 128, "%d", input);

	return buffer;
}

String Convert::toString(unsigned long input)
{
	char buffer[128];
	snprintf(buffer, 128, "%lu", input);

	return buffer;
}

String Convert::toString(double input)
{
	char buffer[128];
	snprintf(buffer, 128, "%f", input);

	return buffer;
}

String Convert::toString(const byte* src, size_t count)
{
	//At least enough space, even if every byte turns out to be a single character
	char output[count + 1];
	int ctr = 0;
	while(*src != 0)
	{
		if((*src & 0x80) == 0)
		{
			//ASCII-compatible character
			output[ctr++] = *src++;
		}
		else if((*src & 0xE0) == 0xC0)
		{
			// 2 bytes
			byte b1 = *src++;
			byte b2 = *src++;
			if((b2 & 0xC0) != 0x80)
					break;
			output[ctr++] = ((b1 & 0x1F) << 6) | (b2 & 0x3F);
		}
		else if((*src & 0xF0) == 0xE0)
		{
			// 3 bytes
			byte b1 = *src++;
			byte b2 = *src++;
			byte b3 = *src++;
			if((b2 & 0xC0) != 0x80)
					break;
			if((b3 & 0xC0) != 0x80)
					break;
			output[ctr++] = ((b1 & 0x0F) << 12) | ((b2 & 0x1F) << 6) |
					(b3 & 0x3F);
		}
	}
	output[ctr] = '\0';

	return output;
}

String Convert::asciiCodeToString(int asciiCode)
{
	String character = "";
	switch(asciiCode) {
	// SPECIAL CHARACTERS
	case 32:
		character = " ";
		break;
	case 33:
		character = "!";
		break;
	case 34:
		character = "\"";
		break;
	case 35:
		character = "#";
		break;
	case 36:
		character = "$";
		break;
	case 37:
		character = "%";
		break;
	case 38:
		character = "&";
		break;
	case 39:
		character = "'";
		break;
	case 40:
		character = "(";
		break;
	case 41:
		character = ")";
		break;
	case 42:
		character = "*";
		break;
	case 43:
		character = "+";
		break;
	//case 44:
	//	character = "";
	//	break;
	case 45:
		character = "-";
		break;
	case 46:
		character = ".";
		break;
	case 47:
		character = "/";
		break;
	case 58:
		character = ":";
		break;
	case 59:
		character = ";";
		break;
	case 60:
		character = "<";
		break;
	case 61:
		character = "=";
		break;
	case 62:
		character = ">";
		break;
	case 63:
		character = "?";
		break;
	case 64:
		character = "@";
		break;
	case 91:
		character = "[";
		break;
	case 92:
		character = "\\";
		break;
	case 93:
		character = "]";
		break;
	case 94:
		character = "^";
		break;
	case 95:
		character = "_";
		break;
	case 96:
		character = "`";
		break;
	case 123:
		character = "{";
		break;
	case 124:
		character = "|";
		break;
	case 125:
		character = "}";
		break;
	case 126:
		character = "~";
		break;

	// NUMBERS
	case 48:
		character = "0";
		break;
	case 49:
		character = "1";
		break;
	case 50:
		character = "2";
		break;
	case 51:
		character = "3";
		break;
	case 52:
		character = "4";
		break;
	case 53:
		character = "5";
		break;
	case 54:
		character = "6";
		break;
	case 55:
		character = "7";
		break;
	case 56:
		character = "8";
		break;
	case 57:
		character = "9";
		break;

	// UPPER CASE CHARACTERS
	case 65:
		character = "A";
		break;
	case 66:
		character = "B";
		break;
	case 67:
		character = "C";
		break;
	case 68:
		character = "D";
		break;
	case 69:
		character = "E";
		break;
	case 70:
		character = "F";
		break;
	case 71:
		character = "G";
		break;
	case 72:
		character = "H";
		break;
	case 73:
		character = "I";
		break;
	case 74:
		character = "J";
		break;
	case 75:
		character = "K";
		break;
	case 76:
		character = "L";
		break;
	case 77:
		character = "M";
		break;
	case 78:
		character = "N";
		break;
	case 79:
		character = "O";
		break;
	case 80:
		character = "P";
		break;
	case 81:
		character = "Q";
		break;
	case 82:
		character = "R";
		break;
	case 83:
		character = "S";
		break;
	case 84:
		character = "T";
		break;
	case 85:
		character = "U";
		break;
	case 86:
		character = "V";
		break;
	case 87:
		character = "W";
		break;
	case 88:
		character = "X";
		break;
	case 89:
		character = "Y";
		break;
	case 90:
		character = "Z";
		break;

	// LOWER CASE CHARACTERS
	case 97:
		character = "a";
		break;
	case 98:
		character = "b";
		break;
	case 99:
		character = "c";
		break;
	case 100:
		character = "d";
		break;
	case 101:
		character = "e";
		break;
	case 102:
		character = "f";
		break;
	case 103:
		character = "g";
		break;
	case 104:
		character = "h";
		break;
	case 105:
		character = "i";
		break;
	case 106:
		character = "j";
		break;
	case 107:
		character = "k";
		break;
	case 108:
		character = "l";
		break;
	case 109:
		character = "m";
		break;
	case 110:
		character = "n";
		break;
	case 111:
		character = "o";
		break;
	case 112:
		character = "p";
		break;
	case 113:
		character = "q";
		break;
	case 114:
		character = "r";
		break;
	case 115:
		character = "s";
		break;
	case 116:
		character = "t";
		break;
	case 117:
		character = "u";
		break;
	case 118:
		character = "v";
		break;
	case 119:
		character = "w";
		break;
	case 120:
		character = "x";
		break;
	case 121:
		character = "y";
		break;
	case 122:
		character = "z";
		break;
	default:
		character = "";
		break;
		}
	return character;
}

int Convert::hexToInt(const char* input)
{

	int v = 0;
	while (char c = *input++)
	{
		if (c < '0') return 0; //invalid character
		if (c > '9') //shift alphabetic characters down
		{
		if (c >= 'a') c -= 'a' - 'A'; //upper-case 'a' or higher
		if (c > 'Z') return 0; //invalid character
		if (c > '9') c -= 'A'-1-'9'; //make consecutive with digits
		if (c < '9' + 1) return 0; //invalid character
		}
		c -= '0'; //convert char to hex digit value
		v = v << 4; //shift left by a hex digit
		v += c; //add the current digit
	}

	return v;
}

