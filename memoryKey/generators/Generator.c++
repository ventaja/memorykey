#include "Generator.h"
/**
 * The interface for accessing a key generator. Any algorithm must use
 * getKey and be accessible here.
 * @Author: Adam Knox
 *
 *      Generator IDs
 *      0:	Prototype
 *      1:	Security Course
 *      2:
 */


Generator::Generator () {}
Generator::~Generator () {}

MAUtil::String Generator::generatePassword (const Password password, const MAUtil::Vector<int> iconSequence) {

	// call the generator associated with id 0
	if (password.idAlgorithm == 0) {
		Prototype protypeAlg;
		return protypeAlg.getKey(password, iconSequence);
	} else if (password.idAlgorithm == 1) {
		SecurityCourse securityCourseAlg;
		return securityCourseAlg.getKey(password, iconSequence);
	}
	//todo: add more generators

	// Invalid algorithm id so return an error
	else {
		return "error 0";
	}
}

