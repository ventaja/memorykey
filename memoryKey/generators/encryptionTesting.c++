#include <conprint.h>


#include "../model/Password.h"
#include "Generator.h"


/**
 * description: Program Entry function for testing encryption algorithms. Comment out when merging back into the master
 * @author Adam Knox
 * @return success/fail
 **/
/* Disabled so that the program will not try to start in two places
extern "C" int MAMain() {

	// Create the password parameters to test SecurityCourse with
	Password* password = new Password("Security Course Test","Test password for the infosec course algorithm",PICTURE_GRID,0,4,4,4,4,0,0);

	// Create the sequence to run the test on
	Sequence* sequence = new Sequence();
	sequence->insert(1);
	sequence->insert(5);
	sequence->insert(3);
	sequence->insert(2);
	sequence->insert(9);
	sequence->insert(2);
	sequence->insert(7);

	// Run the algorithm
	MAUtil::String key = Generators::generatePassword(*password, *sequence);

	// Show results in log file
	printf("RESULT: %s", key.c_str());
	return 0;
}
*/
