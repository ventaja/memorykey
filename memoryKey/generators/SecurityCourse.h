#ifndef SECURITY_COURSE_H
#define SECURITY_COURSE_H

#include <MAUtil/String.h>
#include "../model/Password.h"
#include "../utilities/Convert.h"

class SecurityCourse {
public:
	SecurityCourse();
	virtual ~SecurityCourse ();
	MAUtil::String getKey(const Password password, const MAUtil::Vector<int> iconSequence);

private:
};

#endif
