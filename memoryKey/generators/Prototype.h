#ifndef PROTOTYPE_H
#define PROTOTYPE_H

#include <MAUtil/String.h>
#include <MAUtil/Vector.h>
#include "../model/Password.h"
#include "../utilities/Convert.h"

class Prototype {
public:
	Prototype();
	virtual ~Prototype();
	MAUtil::String getKey(Password password, MAUtil::Vector<int> iconSequence);

private:
	MAUtil::Vector<char> getLowerCaseChars(const MAUtil::Vector<int> iconSequence, int powerIncrements);
	MAUtil::Vector<char> getUpperCaseChars(const MAUtil::Vector<int> iconSequence, int powerIncrements);
	MAUtil::Vector<char> getSpecialChars(const MAUtil::Vector<int> iconSequence, int powerIncrements);
	MAUtil::Vector<char> getNumbers(const MAUtil::Vector<int> iconSequence, int powerIncrements);

	int largestOf(const int a, const int b, const int c, const int d);

	MAUtil::Vector<char> lowerCaseLetters;
	MAUtil::Vector<char> upperCaseLetters;
	MAUtil::Vector<char> numbers;
	MAUtil::Vector<char> special;

	unsigned long lowerCaseLettersNumber;
	unsigned long upperCaseLettersNumber;
	unsigned long specialCharsNumber;
	unsigned long numbersNumber;
};

#endif
