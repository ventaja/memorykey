#include "Prototype.h"
#include <conprint.h>


Prototype::Prototype () {
	lowerCaseLetters.add('h');
	lowerCaseLetters.add('j');
	lowerCaseLetters.add('x');
	lowerCaseLetters.add('a');
	lowerCaseLetters.add('n');
	lowerCaseLetters.add('d');
	lowerCaseLetters.add('p');
	lowerCaseLetters.add('e');
	lowerCaseLetters.add('g');
	lowerCaseLetters.add('q');
	lowerCaseLetters.add('r');
	lowerCaseLetters.add('y');
	lowerCaseLetters.add('f');
	lowerCaseLetters.add('u');
	lowerCaseLetters.add('k');
	lowerCaseLetters.add('l');
	lowerCaseLetters.add('z');
	lowerCaseLetters.add('b');
	lowerCaseLetters.add('c');
	lowerCaseLetters.add('t');
	lowerCaseLetters.add('m');
	lowerCaseLetters.add('v');
	lowerCaseLetters.add('w');
	lowerCaseLetters.add('i');
	lowerCaseLetters.add('o');
	lowerCaseLetters.add('s');

	upperCaseLetters.add('X');
	upperCaseLetters.add('Y');
	upperCaseLetters.add('D');
	upperCaseLetters.add('W');
	upperCaseLetters.add('T');
	upperCaseLetters.add('U');
	upperCaseLetters.add('L');
	upperCaseLetters.add('K');
	upperCaseLetters.add('O');
	upperCaseLetters.add('P');
	upperCaseLetters.add('S');
	upperCaseLetters.add('B');
	upperCaseLetters.add('A');
	upperCaseLetters.add('H');
	upperCaseLetters.add('Q');
	upperCaseLetters.add('I');
	upperCaseLetters.add('F');
	upperCaseLetters.add('J');
	upperCaseLetters.add('G');
	upperCaseLetters.add('N');
	upperCaseLetters.add('V');
	upperCaseLetters.add('R');
	upperCaseLetters.add('C');
	upperCaseLetters.add('E');
	upperCaseLetters.add('Z');
	upperCaseLetters.add('M');

	numbers.add('4');
	numbers.add('1');
	numbers.add('3');
	numbers.add('8');
	numbers.add('0');
	numbers.add('5');
	numbers.add('6');
	numbers.add('9');
	numbers.add('7');
	numbers.add('2');

	special.add('`');
	special.add('~');
	special.add('!');
	special.add('@');
	special.add('#');
	special.add('$');
	special.add('%');
	special.add('^');
	special.add('&');
	special.add('*');
	special.add('(');
	special.add(')');
	special.add('_');
	special.add('-');
	special.add('+');
	special.add('=');
	special.add('{');
	special.add('[');
	special.add('}');
	special.add(']');
	special.add('|');
	special.add('\\');
	special.add(':');
	special.add(';');
	special.add('\"');
	special.add('\'');
	special.add('<');
	special.add(',');
	special.add('>');
	special.add('.');
	special.add('?');
	special.add('/');


}
Prototype::~Prototype () {}

MAUtil::String Prototype::getKey(const Password password, const MAUtil::Vector<int> iconSequence) {

	MAUtil::Vector<char> usedLowerCaseLetters;
	MAUtil::Vector<char> usedUpperCaseLetters;
	MAUtil::Vector<char> usedSpecialChars;
	MAUtil::Vector<char> usedNumbers;
	MAUtil::String key = "";

	/// Get all the characters needed to form the key
	// Get the set of lower case characters to use
	for (int power = 0; usedLowerCaseLetters.size() < password.numRequiredLowerCase; power++) {
		usedLowerCaseLetters = getLowerCaseChars(iconSequence, power);
	}
	for(int i = 0; i < usedLowerCaseLetters.size(); i++) {
	}

	// Get the set of upper case characters to use
	for (int power = 0; usedUpperCaseLetters.size() < password.numRequiredUpperCase; power++) {
		usedUpperCaseLetters = getUpperCaseChars(iconSequence, power);
	}
	for(int i = 0; i < usedUpperCaseLetters.size(); i++) {
	}

	// Get the set of special characters to use
	for (int power = 0; usedSpecialChars.size() < password.numRequiredSpecialCharacters; power++) {
		usedSpecialChars = getSpecialChars(iconSequence, power);
	}
	for(int i = 0; i < usedSpecialChars.size(); i++) {
	}

	// Get the set of numbers to use
	for (int power = 0; usedNumbers.size() < password.numRequiredNumbers; power++) {
		usedNumbers = getNumbers(iconSequence, power);
	}
	for(int i = 0; i < usedNumbers.size(); i++) {
	}

	/// Form the key
	unsigned int numCharacters = usedLowerCaseLetters.size() + usedUpperCaseLetters.size() + usedNumbers.size() + usedSpecialChars.size();

	// Create a set of number strings, one for each character type being used
	MAUtil::String lowerCaseLettersNumString = Convert::toString(lowerCaseLettersNumber);
	MAUtil::String upperCaseLettersNumString = Convert::toString(upperCaseLettersNumber);
	MAUtil::String specialCharsNumString = Convert::toString(specialCharsNumber);
	MAUtil::String numbersNumString = Convert::toString(numbersNumber);


	// Add characters
	bool lowerPaused = false;
	bool upperPaused = false;
	bool numberPaused = false;
	bool specialPaused = false;

	// Each character type is paused from being added to the set once its minimum requirement is fulfilled
	// After all 4 have reached their minimums, all are activated again until the characters rut out
	// Finally, the maximum password length is obtained by taking a substring of the key
	int largestSet = largestOf(usedLowerCaseLetters.size(), usedUpperCaseLetters.size(), usedNumbers.size(), usedSpecialChars.size());
	for (int i = 0; i < largestSet; i++) {
		if (i < usedLowerCaseLetters.size() && !lowerPaused) {
			if ((Convert::toInt(lowerCaseLettersNumString.substr(i,1)) <= 4) && (key.size() > 0)) {
				key.insert(1, usedLowerCaseLetters[i]);
			} else {
				key.append(&usedLowerCaseLetters[i], 1);
			}

			if (i == password.numRequiredLowerCase) {
				lowerPaused = true;
			}
		}

		if (i < usedUpperCaseLetters.size() && !upperPaused) {
			if ((Convert::toInt(upperCaseLettersNumString.substr(i,1)) <= 4) && (key.size() > 0)) {
				key.insert(1, usedUpperCaseLetters[i]);
			} else {
				key.append(&usedUpperCaseLetters[i], 1);
			}

			if (i == password.numRequiredUpperCase) {
				upperPaused = true;
			}
		}

		if (i < usedSpecialChars.size() && !numberPaused) {
			if ((Convert::toInt(specialCharsNumString.substr(i,1)) <= 4) && (key.size() > 0)) {
				key.insert(1, usedSpecialChars[i]);
			} else {
				key.append(&usedSpecialChars[i], 1);
			}

			if (i == password.numRequiredSpecialCharacters) {
				specialPaused = true;
			}
		}

		if (i < usedNumbers.size() && !specialPaused) {
			if ((Convert::toInt(numbersNumString.substr(i,1)) <= 4) && (key.size() > 0)) {
				key.insert(1, usedNumbers[i]);
			} else {
				key.append(&usedNumbers[i], 1);
			}

			if (i == password.numRequiredNumbers) {
				numberPaused = true;
			}
		}

		// Start up character entry for all categories again since min requirements are now met
		if (lowerPaused && upperPaused && numberPaused && specialPaused) {
			lowerPaused = false;
			upperPaused = false;
			numberPaused = false;
			specialPaused = false;
		}
	}

	// return substring if key is too long
	if (key.size() > password.maxPasswordLength) {
		return key.substr(0, password.maxPasswordLength);
	} else {
		return key;
	}
}

MAUtil::Vector<char> Prototype::getLowerCaseChars(const MAUtil::Vector<int> iconSequence, int powerIncrements) {
	lowerCaseLettersNumber = 1;
	int initialPower = 1;

	MAUtil::Vector<int> seqPowersList;
	MAUtil::Vector<char> usedLowerCaseLetters;
	MAUtil::Vector<int> lowerCaseLetterCodesList;// = new MAUtil::Vector<MAUtil::String> ();
	/// Get a string of numbers that has enough numbers to produce the number length needed
	// Set the initial powers
	if (powerIncrements / iconSequence.size() >= 1) {
		initialPower = (powerIncrements / iconSequence.size()) + 1;
		powerIncrements = powerIncrements % iconSequence.size();
	}
	for (int i = 0; i < iconSequence.size(); i++) {
		seqPowersList.add(initialPower);
	}
	// Increment each exponent that still needs incrementing
	for (int i = 0; powerIncrements > 0; i++, powerIncrements--) {
		if (i >= seqPowersList.size())
			i = 0;
		seqPowersList[i]++;
	}

	// run sequence numbers through algorithm
	for (int i = 0; i < iconSequence.size(); i++) {
		int imgNumber 	= iconSequence[i];
		int power		= seqPowersList[i];

		//Form the number
		lowerCaseLettersNumber *= imgNumber^power;
	}

	// Convert the number into a string
	MAUtil::String lowerCaseLettersString = Convert::toString(lowerCaseLettersNumber);

	// Can only run this part if there is a long enough number to work with
	if (lowerCaseLettersString.length() > 1) {
		// split the number into a set of integers that are from 1 to 26
		for (int i = 0; i < lowerCaseLettersString.length() - 1; i++) {
			// compare the split
			if (Convert::toInt(lowerCaseLettersString.substr(i, 2).c_str()) < 26) {
				lowerCaseLetterCodesList.add(Convert::toInt(lowerCaseLettersString.substr(i, 2).c_str()));
			}
		}

		// Convert the list of 2 digit numbers into a set of characters
		for (int i = 0; i < lowerCaseLetterCodesList.size(); i++) {
			usedLowerCaseLetters.add(lowerCaseLetters[lowerCaseLetterCodesList[i]]);
		}
	}

	// Return the list of lower case characters to be used in the key
	return usedLowerCaseLetters;
}





MAUtil::Vector<char> Prototype::getUpperCaseChars(const MAUtil::Vector<int> iconSequence, int powerIncrements) {
	upperCaseLettersNumber = 1;
	int initialPower = 1;

	MAUtil::Vector<int> seqPowersList;
	MAUtil::Vector<char> usedUpperCaseLetters;
	MAUtil::Vector<int> upperCaseLetterCodesList;// = new MAUtil::Vector<MAUtil::String> ();
	/// Get a string of numbers that has enough numbers to produce the number length needed
	// Set the initial powers
	if (powerIncrements / iconSequence.size() >= 1) {
		initialPower = (powerIncrements / iconSequence.size()) + 1;
		powerIncrements = powerIncrements % iconSequence.size();
	}
	for (int i = 0; i < iconSequence.size(); i++) {
		seqPowersList.add(initialPower);
	}
	// Increment each exponent that still needs incrementing
	for (int i = 0; powerIncrements > 0; i++, powerIncrements--) {
		if (i >= seqPowersList.size())
			i = 0;
		seqPowersList[i]++;
	}

	// run sequence numbers through algorithm
	for (int i = 0; i < iconSequence.size(); i++) {
		int imgNumber 	= iconSequence[i];
		int power		= seqPowersList[i];

		//Form the number
		upperCaseLettersNumber *= imgNumber^power;
	}

	// Convert the number into a string
	MAUtil::String upperCaseLettersString = Convert::toString(upperCaseLettersNumber);

	// Can only run this part if there is a long enough number to work with
	if (upperCaseLettersString.length() > 1) {
		// split the number into a set of integers that are from 1 to 26
		for (int i = 0; i < upperCaseLettersString.length() - 1; i++) {
			// compare the split
			if (Convert::toInt(upperCaseLettersString.substr(i, 2).c_str()) < 26) {
				upperCaseLetterCodesList.add(Convert::toInt(upperCaseLettersString.substr(i, 2).c_str()));
			}
		}

		// Convert the list of 2 digit numbers into a set of characters
		for (int i = 0; i < upperCaseLetterCodesList.size(); i++) {
			usedUpperCaseLetters.add(upperCaseLetters[upperCaseLetterCodesList[i]]);
		}
	}

	// Return the list of upper case characters to be used in the key
	return usedUpperCaseLetters;
}





MAUtil::Vector<char> Prototype::getSpecialChars(const MAUtil::Vector<int> iconSequence, int powerIncrements) {
	specialCharsNumber = 1;
	int initialPower = 1;

	MAUtil::Vector<int> seqPowersList;
	MAUtil::Vector<char> usedSpecialChars;
	MAUtil::Vector<int> specialCharCodesList;
	/// Get a string of numbers that has enough numbers to produce the number length needed
	// Set the initial powers
	if (powerIncrements / iconSequence.size() >= 1) {
		initialPower = (powerIncrements / iconSequence.size()) + 1;
		powerIncrements = powerIncrements % iconSequence.size();
	}
	for (int i = 0; i < iconSequence.size(); i++) {
		seqPowersList.add(initialPower);
	}
	// Increment each exponent that still needs incrementing
	for (int i = 0; powerIncrements > 0; i++, powerIncrements--) {
		if (i >= seqPowersList.size())
			i = 0;
		seqPowersList[i]++;
	}

	// run sequence numbers through algorithm
	for (int i = 0; i < iconSequence.size(); i++) {
		int imgNumber 	= iconSequence[i];
		int power		= seqPowersList[i];

		//Form the number
		specialCharsNumber *= imgNumber^power;
	}

	// Convert the number into a string
	MAUtil::String specialCharsString = Convert::toString(specialCharsNumber);

	// Can only run this part if there is a long enough number to work with
	if (specialCharsString.length() > 1) {
		// split the number into a set of integers that are from 1 to 32
		for (int i = 0; i < specialCharsString.length() - 1; i++) {
			// compare the split
			if (Convert::toInt(specialCharsString.substr(i, 2).c_str()) < 32) {
				specialCharCodesList.add(Convert::toInt(specialCharsString.substr(i, 2).c_str()));
			}
		}

		// Convert the list of 2 digit numbers into a set of characters
		for (int i = 0; i < specialCharCodesList.size(); i++) {
			usedSpecialChars.add(special[specialCharCodesList[i]]);
		}
	}

	// Return the list of upper case characters to be used in the key
	return usedSpecialChars;
}





MAUtil::Vector<char> Prototype::getNumbers(const MAUtil::Vector<int> iconSequence, int powerIncrements) {

	numbersNumber = 1;
	int initialPower = 1;

	MAUtil::Vector<int> seqPowersList;
	MAUtil::Vector<char> usedNumbers;
	MAUtil::Vector<int> numbersCodesList;

	/// Get a string of numbers that has enough numbers to produce the number length needed
	// Set the initial powers
	if (powerIncrements / iconSequence.size() >= 1) {
		initialPower = (powerIncrements / iconSequence.size()) + 1;
		powerIncrements = powerIncrements % iconSequence.size();
	}
	for (int i = 0; i < iconSequence.size(); i++) {
		seqPowersList.add(initialPower);
	}
	// Increment each exponent that still needs incrementing
	for (int i = 0; powerIncrements > 0; i++, powerIncrements--) {
		if (i >= seqPowersList.size())
			i = 0;
		seqPowersList[i]++;
	}

	// run sequence numbers through algorithm
	for (int i = 0; i < iconSequence.size(); i++) {
		int imgNumber 	= iconSequence[i];
		int power		= seqPowersList[i];

		//Form the number
		numbersNumber *= imgNumber^power;
	}

	// Convert the number into a string
	MAUtil::String numbersString = Convert::toString(numbersNumber);

	// Can only run this part if there is a long enough number to work with
	if (numbersString.length() > 1) {
		// split the number into a set of integers that are from 0 to 9
		for (int i = 0; i < numbersString.length() - 1; i++) {
			numbersCodesList.add(Convert::toInt(numbersString.substr(i, 1).c_str()));
		}

		// Convert the list of 2 digit numbers into a set of characters
		for (int i = 0; i < numbersCodesList.size(); i++) {
			usedNumbers.add(numbers[numbersCodesList[i]]);
		}
	}

	// Return the list of numbers to be used in the key
	return usedNumbers;
}

int Prototype::largestOf(const int a, const int b, const int c, const int d) {
	int largest = a;
	if (largest < b)
		largest = b;
	if (largest < c)
		largest = c;
	if (largest < d)
		largest = d;
	return largest;
}




