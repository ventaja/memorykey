#ifndef GENERATOR_H
#define GENERATOR_H

#include <MAUtil/String.h>
#include "Prototype.h"
#include "SecurityCourse.h"

class Generator {
public:
	Generator ();
	virtual ~Generator ();
	MAUtil::String generatePassword(const Password password, const MAUtil::Vector<int> iconSequence);
};

#endif
