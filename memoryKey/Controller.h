/**
 * description: The main moblet that boots the main modules,
 * 				and handles any communication between them.
 * @author: Adam Knox
 **/


#ifndef CONTROLLER_H
#define CONTROLLER_H


// Included Libraries
#include <maapi.h>
#include <MAUI/Font.h>
#include <MAUI/Label.h>
#include <MAUI/Layout.h>
#include <MAUI/Screen.h>
#include <MAUtil/String.h>


// Included Project Headers
#include "generators/Generator.h"
#include "model/Model.h"
#include "ui/Input_PictureGrid.h"


// Forward declarations
// Forward declaration needed to create objects
// for classes that reference each other
class UI;


// Controller Class Declaration
class Controller
{
public:
	/**
	 * Constructor: Boots the controller module
	 * @author Adam Knox
	 */
	Controller();

	/**
	 * Destructor
	 * @author Adam Knox
	 */
	virtual ~Controller();

	/**
	 * Adds a password to the user's existing list of passwords
	 * @author Adam Knox
	 * @param password
	 */
	bool createPassword(const Password password);

	bool editPassword(const Password oldPassword, const Password newPassword);

	void deletePassword(const Password password);

	/**
	 * Displays the chosen window. Does not display input screens
	 * @author Adam Knox
	 * @param window the window to display. Cannot be a screen prefixed with 'Input'
	 */
	void showScreen(const Windows window);

	void showScreen(const Windows window, const Password password);

	/**
	 * Displays the required sequence input screen
	 * @author Adam Knox
	 * @param password The object containing the constraint data for the algorithm
	 */
	void showScreen(const Password password);

	/**
	 * Runs the algorithm for generating a password then shows the password
	 * @author Adam Knox
	 * @param password The object containing the constraint data for the algorithm
	 * @param sequence The sequence of data input by the user from a sequence input screen
	 */
	bool showKey(const Password password, const MAUtil::Vector<int> iconSequence);

	void applyLicense(const char* license);

private:
	bool licenseValid();

};

#endif
