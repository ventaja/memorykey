/**
 * description: The main moblet that boots the main modules,
 * 				and handles any communication between them.
 * @author: Adam Knox
 **/


#include "Controller.h"


/**
 * Constructor: Boots the controller module
 * @author Adam Knox
 */
Controller::Controller() {}





/**
 * Destructor
 * @author Adam Knox
 */
Controller::~Controller() {}






/**
 * Adds a password to the user's existing list of passwords
 * @author Adam Knox
 * @param password
 */
bool Controller::createPassword(const Password password) {

	if (password.numRequiredNumbers <= Settings::MAX_NUMBERS && password.numRequiredNumbers > 0) {
		if (password.numRequiredSpecialCharacters <= Settings::MAX_SPECIAL_CHARS && password.numRequiredSpecialCharacters > 0) {
			if (password.numRequiredLowerCase <= Settings::MAX_LOWER_CASE && password.numRequiredLowerCase > 0) {
				if (password.numRequiredUpperCase <= Settings::MAX_UPPER_CASE && password.numRequiredUpperCase > 0) {
					if (password.maxPasswordLength <= Settings::MAX_LENGTH && password.maxPasswordLength >= (password.numRequiredNumbers + password.numRequiredSpecialCharacters + password.numRequiredLowerCase + password.numRequiredUpperCase)) {
						// Check that the name does not already exist
						MAUI::Vector<Password> passwords = MODEL->getPasswords();
						for (int i = 0; i < passwords.size(); i++) {
							// make sue the
							if (strcmp(passwords[i].name, password.name) == 0) {
								return false;
							}
						}

						// Sends the password to be added to the model
						MODEL->addPassword(password);
						return true;
					}
				}
			}
		}
	}

	return false;
}




/**
 * Adds a password to the user's existing list of passwords
 * @author Adam Knox
 * @param password
 */
bool Controller::editPassword(const Password oldPassword, const Password newPassword) {
	if (newPassword.numRequiredNumbers <= Settings::MAX_NUMBERS && newPassword.numRequiredNumbers > 0) {
		if (newPassword.numRequiredSpecialCharacters <= Settings::MAX_SPECIAL_CHARS && newPassword.numRequiredSpecialCharacters > 0) {
			if (newPassword.numRequiredLowerCase <= Settings::MAX_LOWER_CASE && newPassword.numRequiredLowerCase > 0) {
				if (newPassword.numRequiredUpperCase <= Settings::MAX_UPPER_CASE && newPassword.numRequiredUpperCase > 0) {
					if (newPassword.maxPasswordLength <= Settings::MAX_LENGTH && newPassword.maxPasswordLength >= (newPassword.numRequiredNumbers + newPassword.numRequiredSpecialCharacters + newPassword.numRequiredLowerCase + newPassword.numRequiredUpperCase)) {
						// Check that the name does not already exist
						MAUI::Vector<Password> passwords = MODEL->getPasswords();
						// only worry about the name being the same as one other than the old name
						if (!(strcmp(newPassword.name, oldPassword.name) == 0)) {
							for (int i = 0; i < passwords.size(); i++) {
								if (strcmp(passwords[i].name, newPassword.name) == 0) {
									return false;
								}
							}
						}

						// Update the model
						MODEL->editPassword(oldPassword, newPassword);
						return true;
					}
				}
			}
		}
	}

	return false;
}


/**
 * Adds a password to the user's existing list of passwords
 * @author Adam Knox
 * @param password
 */
void Controller::deletePassword(const Password password) {
	MODEL->removePassword(password);
}





/**
 * Displays the chosen window. Does not display input screens
 * @author Adam Knox
 * @param window the window to display. Cannot be a screen prefixed with 'Input'
 */
void Controller::showScreen(const Windows window) {
	// show one of the screens
	switch(window) {
		case HOME:
			if (Settings::MULTIPLE_PASSWORDS) {
				MODEL->menuScreen->loadPasswords();	// Updates the list of passwords displayed in the menu
				MODEL->menuScreen->show();
			} else {
				MODEL->inputScreen_PictureGrid->open(*MODEL->getPasswords().begin());
			}
			break;

		case CREATE_PASSWORD:
			MODEL->createPasswordScreen->open();
			break;

		case RENEW_LICENSE:
			MODEL->renewLicenseScreen->open();
			break;

		default:
			break;
	}
}


void Controller::showScreen(const Windows window, const Password password) {
	switch(window) {
		case EDIT_PASSWORD:
			MODEL->editPasswordScreen->open(password);
			break;

		default:
			break;
	}
}




/**
 * Displays the required sequence input screen
 * @author Adam Knox
 * @param password The object containing the constraint data for the algorithm
 */
void Controller::showScreen(const Password password) {
	// Show one of the sequence input screens
	switch(password.idInputMethod) {
		case PICTURE_GRID:
			MODEL->inputScreen_PictureGrid->open(password);
			break;
	}
}





/**
 * Runs the algorithm for generating a password then shows the password
 * @author Adam Knox
 * @param password The object containing the constraint data for the algorithm
 * @param sequence The sequence of data input by the user from a sequence input screen
 */
bool Controller::showKey(const Password password, const MAUtil::Vector<int> iconSequence) {
	if (licenseValid() && iconSequence.size() >= Settings::MIN_ICON_SEQUENCE_LENGTH) {
		//Run the algorithm to get the key
		Generator keyGenerator;
		MAUtil::String key = keyGenerator.generatePassword(password, iconSequence);

		//Display the key in the key display screen
		MODEL->viewKeyScreen->showKey(key);
		MODEL->viewKeyScreen->show();
		key.clear();

		return true;
	} else {
		return false;
	}
}


void Controller::applyLicense(const char* license) {
	MODEL->setLicense(license);
}



bool Controller::licenseValid () {

	// Check if the alpha license is valid
	if (strcmp(MODEL->getLicense().c_str(), "Leftover Pineapple") == 0) {
		return true;
	}

	else if (strcmp(MODEL->getLicense().c_str(), "Alpha") == 0) {
		// Check if the date for the end of alpha testing has passed
		 struct tm * tim_p = new tm;
		split_time(maTime(), tim_p);
		/*
		printf("system hour is %d ", tim_p->tm_hour);
		printf("system min is %d ", tim_p->tm_min);
		printf("system sec is %d ", tim_p->tm_sec);
		printf("Daylight Saving Time flag %d ", tim_p->tm_isdst);
		printf("Day of the month %d ", tim_p->tm_mday);
		printf("Months since January %d ", tim_p->tm_mon);
		printf("Days since Sunday %d ", tim_p->tm_wday);
		printf("Days since January 1 %d ", tim_p->tm_yday);
		printf("Years since 1900 %d ", tim_p->tm_year);
		 */
		if ((tim_p->tm_year == Settings::TESTING_YEAR - 1900) && (tim_p->tm_mon < Settings::TESTING_MONTH)) {
			delete tim_p;
			return true;
		}
		delete tim_p;
	}

	return false;
}
