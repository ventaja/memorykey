/**
 * The screen used for setting up a new password
 * @author Adam Knox
 */


#ifndef CREATE_PASSWORD_H
#define CREATE_PASSWORD_H


// Included Libraries
// #include <conprint.h>
#include <MAUI/Screen.h>
#include <MAUI/Label.h>
#include <MAUI/Layout.h>
#include <MAUI/EditBox.h>
#include <MAUI/Font.h>
#include <ma.h>


// Included Project Headers
#include "../Controller.h"
#include "../model/Enum.h"
#include "../model/Password.h"
#include "../utilities/Convert.h"
#include "../utilities/KineticListBox.h"
#include "Theme.h"


// Need to include this if anything is pulled from the resources folder
#include "MAHeaders.h"


// Forward declarations


// Set Password Window Class Declaration
class CreatePassword :
	public MAUI::Screen,
	public MAUI::WidgetListener
{
public:
	/**
	 * Constructor: Builds the basic layout, then calls the proper function to build the customized part of the layout.
	 * @author Adam Knox
	 * @param ui The address to the controller of this screen
	 * @param window The type of window to display
	 */
		CreatePassword();

	/**
	 * Destructor
	 * @author Adam Knox
	 */
	virtual ~CreatePassword();

	/**
	 * sets the fields to contain the stored password data or no password data for a new password
	 * @author Adam Knox
	 */
	void open();

private:

	/**
	 * Runs the action for the selected widget
	 * @author Adam Knox
	 * @return Completion Status
	 */
	int runWidgetAction();

	/**
	 * Listens for any key presses and does the associated action
	 * @author Adam Knox
	 * @param keyCode The code for the key pressed
	 */
	void keyPressEvent(int keyCode, int nativeCode);

	//todo: add header comment
	void keyboardToEditBox(const int keyCode);

	/**
	 * Listens for change in selection status of widgets on screen to
	 * make sure the editboxes are selected instead of the labels
	 * @author Adam Knox
	 * @param widget The widget that had its status chaged
	 * @param selected the status of the widget
	 */
	void selectionChanged(const MAUI::Widget* widget, const bool selected);

	/**
	 * Listens for a press on the screen and runs proper action for the press
	 * @author Adam Knox
	 * @param point The position on the screen that is being pressed
	 */
	void pointerPressEvent(const MAPoint2d point);

	/**
	 * Listens for the release of a point on the screen and runs proper action for the release
	 * @author Adam Knox
	 * @param point The position on the screen that is being released
	 */
	void pointerReleaseEvent(const MAPoint2d point);

	// Heap Pointers
	MAUI::Label* 					headerLabel;		// A text label that displays instructions
	MAUI::Label* 					createButton;		// Create Password Button
	MAUI::Layout* 					mainLayout;			// The main layout that holds the other widgets
	MAUI::KineticListBox* 			body;				// Scrolling area containing the labels and text boxes
	MAUtil::Vector<MAUI::Label*> 	paramContainersList;	// List of the parameter container objects (labels)
	MAUtil::Vector<MAUI::Label*> 	paramLabelsList;		// List of the fields within the containers for writing text in
	MAUtil::Vector<MAUI::EditBox*> 	paramFieldsList;		// List of the edit boxes in the containers for entering data into
	Theme* 							theme;				// The pointer to theme information

	// Stack Data
	MAPoint2d						downPoint;			// cursor position on touch screen press
	unsigned int					bodyIndex;
};

#endif
