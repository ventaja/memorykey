#ifndef THEME_H
#define THEME_H


#include <MAUI/Font.h>
#include <MAUI/WidgetSkin.h>

// Need to include MA Headers if anything is pulled from the resources folder
#include "MAHeaders.h"

class Theme {
public:
	Theme();
	virtual ~Theme();

	const static unsigned int keyboardHeight = 350;

	unsigned int displayHeight;
	unsigned int displayWidth;

	// Layout
	unsigned int 		layoutHeight;
	unsigned int 		layoutWidth;
	unsigned int 		layoutBGColor;
	MAUI::WidgetSkin* 	layoutSkin;

	// Body
	unsigned int 		bodyPadding;
	unsigned int 		bodyHeight;
	unsigned int 		bodyWidth;
	unsigned int 		bodyBGColor;
	MAUI::WidgetSkin* 	bodySkin;

	// Grid
	unsigned int		gridBGColorUp;
	unsigned int		gridBGColorDown;

	// Labels
	// Labels: Header
	unsigned int 		headerHeight;
	unsigned int 		headerWidth;
	unsigned int 		headerBGColor;
	MAUI::Font* 		headerFont;
	MAUI::WidgetSkin* 	headerSkin;

	// Labels: Buttons
	unsigned int 		buttonHeight;
	unsigned int 		buttonWidth;
	unsigned int 		button1BGColor;
	unsigned int 		button2BGColor;
	unsigned int 		button3BGColor;
	MAUI::Font* 		buttonFont;
	MAUI::WidgetSkin* 	button1Skin;
	MAUI::WidgetSkin* 	button2Skin;
	MAUI::WidgetSkin* 	button3Skin;

	// Labels: EditBox Container
	unsigned int 		ebContainerHeight;
	unsigned int 		ebContainerWidth;
	unsigned int		ebContainerBGColor;
	MAUI::Font* 		ebContainerFont;
	MAUI::WidgetSkin* 	ebContainerSkin;

	// Labels: EditBox Label
	unsigned int 		ebLabelXOffset;
	unsigned int 		ebLabelYOffset;
	unsigned int 		ebLabelHeight;
	unsigned int 		ebLabelWidth;
	unsigned int		ebLabelBGColor;
	MAUI::Font* 		ebLabelFont;
	MAUI::WidgetSkin* 	ebLabelSkin;

	// Labels: Display Key Text
	unsigned int 		displayKeyHeight;
	unsigned int 		displayKeyWidth;
	unsigned int		displayKeyBGColor;
	MAUI::Font* 		displayKeyFont;
	MAUI::WidgetSkin* 	displayKeySkin;

	// Labels: Text
	unsigned int 		textHeight;
	unsigned int 		textWidth;
	unsigned int 		textBGColor;
	MAUI::Font* 		textFont;
	MAUI::WidgetSkin* 	textSkin;

	// EditBox
	unsigned int 		editBoxXOffset;
	unsigned int 		editBoxYOffset;
	unsigned int 		editBoxHeight;
	unsigned int 		editBoxWidth;
	unsigned int		editBoxBGColor;
	MAUI::Font* 		editBoxFont;
	MAUI::WidgetSkin* 	editBoxSkin;

private:
};
#endif
