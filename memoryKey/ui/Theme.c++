#include "Theme.h"


Theme::Theme () {
	// Display
	displayWidth = EXTENT_X(maGetScrSize());
	displayHeight = EXTENT_Y(maGetScrSize());

	// Layouts
	layoutHeight	= displayHeight;
	layoutWidth		= displayWidth;
	layoutBGColor	= 0x8194ad;
	layoutSkin		= NULL;/*new MAUI::WidgetSkin(
			LAYOUT_SKIN,   	// Image for selected state.
			LAYOUT_SKIN, 	// Image for unselected state.
			1, 				// X coordinate for start of center patch.
			51,				// X coordinate for end of center patch.
			1, 				// Y coordinate for start of center patch.
			51,		 		// Y coordinate for end of center patch.
			true,  			// Is selected image transparent?
			true); 			// Is unselected image transparent?
*/
	// Headers
	headerHeight	= 54;
	headerWidth		= layoutWidth;
	headerBGColor	= 0;
	headerFont		= new MAUI::Font(HEADER);
	headerSkin 		= new MAUI::WidgetSkin(
			HEADER_SKIN,	// Image for selected state.
			HEADER_SKIN, 	// Image for unselected state.
			1, 				// X coordinate for start of center patch.
			3,				// X coordinate for end of center patch.
			1, 				// Y coordinate for start of center patch.
			3,		 		// Y coordinate for end of center patch.
			true,  			// Is selected image transparent?
			true); 			// Is unselected image transparent?

	// Bodies
	bodyPadding		= 10;
	bodyHeight		= layoutHeight - headerHeight - 2*bodyPadding;
	bodyWidth		= layoutWidth - 2*bodyPadding;
	bodyBGColor		= 0x91a4bd;
	bodySkin		= NULL;/*new MAUI::WidgetSkin(
			BODY_SKIN,		// Image for selected state.
			BODY_SKIN, 		// Image for unselected state.
			4, 				// X coordinate for start of center patch.
			8,				// X coordinate for end of center patch.
			4, 				// Y coordinate for start of center patch.
			8,		 		// Y coordinate for end of center patch.
			true,  			// Is selected image transparent?
			true); 			// Is unselected image transparent?*/

	// Grid
	gridBGColorUp	= 0x91a4bd;
	gridBGColorDown	= 0x71849d;

	// Buttons
	buttonHeight	= 100;
	buttonWidth		= bodyWidth;
	button1BGColor	= 0;
	button2BGColor	= 0;
	button3BGColor	= 0;
	buttonFont		= new MAUI::Font(BUTTON);
	button1Skin		= new MAUI::WidgetSkin(
			BUTTON1_DOWN_SKIN,	// Image for selected state.
			BUTTON1_UP_SKIN,	// Image for unselected state.
			16, 				// X coordinate for start of center patch.
			20,					// X coordinate for end of center patch.
			16, 				// Y coordinate for start of center patch.
			20,		 			// Y coordinate for end of center patch.
			true,  				// Is selected image transparent?
			true); 				// Is unselected image transparent?
	button2Skin		= new MAUI::WidgetSkin(
			BUTTON2_DOWN_SKIN,	// Image for selected state.
			BUTTON2_UP_SKIN,	// Image for unselected state.
			16, 				// X coordinate for start of center patch.
			20,					// X coordinate for end of center patch.
			16, 				// Y coordinate for start of center patch.
			20,		 			// Y coordinate for end of center patch.
			true,  				// Is selected image transparent?
			true); 				// Is unselected image transparent?
	button3Skin		= new MAUI::WidgetSkin(
			BUTTON3_DOWN_SKIN,	// Image for selected state.
			BUTTON3_UP_SKIN,	// Image for unselected state.
			16, 				// X coordinate for start of center patch.
			20,					// X coordinate for end of center patch.
			16, 				// Y coordinate for start of center patch.
			20,		 			// Y coordinate for end of center patch.
			true,  				// Is selected image transparent?
			true); 				// Is unselected image transparent?

	// Display Key Text
	displayKeyHeight	= bodyHeight - buttonHeight;
	displayKeyWidth		= displayWidth;
	displayKeyBGColor	= 0;
	displayKeyFont		= new MAUI::Font(HEADER);
	displayKeySkin		= NULL;

	// Text Window
	textHeight			= 30;
	textWidth			= displayWidth;
	textBGColor			= 0;
	textFont			= new MAUI::Font(TEXT);
	textSkin			= NULL;


	// Labels: EditBox Container
	ebContainerHeight	= 112;
	ebContainerWidth	= bodyWidth;
	ebContainerBGColor	= 0;
	ebContainerFont		= NULL;
	ebContainerSkin		= NULL;

	// Labels: EditBox Label
	ebLabelXOffset		= 10;
	ebLabelYOffset		= 15;
	ebLabelHeight		= 46;
	ebLabelWidth		= ebContainerWidth - 2 * ebLabelXOffset;
	ebLabelBGColor		= 0x606e82;
	ebLabelFont			= buttonFont;
	ebLabelSkin			= NULL;/*= new MAUI::WidgetSkin(
			EDITBOXLABEL_SKIN,	// Image for selected state.
			EDITBOXLABEL_SKIN,	// Image for unselected state.
			3, 					// X coordinate for start of center patch.
			5,					// X coordinate for end of center patch.
			3, 					// Y coordinate for start of center patch.
			5,		 			// Y coordinate for end of center patch.
			true,  				// Is selected image transparent?
			true); 				// Is unselected image transparent?
*/
	// EditBox
	editBoxXOffset		= 10;
	editBoxYOffset		= ebLabelHeight + ebLabelYOffset;
	editBoxHeight		= 46;
	editBoxWidth		= ebContainerWidth - 2 * editBoxXOffset;
	editBoxBGColor		= 0xa8b4c5;
	editBoxFont			= buttonFont;
	editBoxSkin			= NULL;/*= new MAUI::WidgetSkin(
			EDITBOX_SKIN,	// Image for selected state.
			EDITBOX_SKIN,	// Image for unselected state.
			2, 				// X coordinate for start of center patch.
			5,				// X coordinate for end of center patch.
			1, 				// Y coordinate for start of center patch.
			3,		 		// Y coordinate for end of center patch.
			true,  			// Is selected image transparent?
			true); 			// Is unselected image transparent?
*/
}


Theme::~Theme () {
	delete layoutSkin;
	delete bodySkin;
	delete headerFont;
	delete headerSkin;
	delete buttonFont;
	delete button1Skin;
	delete button2Skin;
	delete button3Skin;
	delete ebContainerFont;
	delete ebContainerSkin;
	delete ebLabelFont;
	delete ebLabelSkin;
	delete displayKeyFont;
	delete displayKeySkin;
	delete textFont;
	delete textSkin;
	delete editBoxFont;
	delete editBoxSkin;
}
