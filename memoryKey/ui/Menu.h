/**
 * description: Builds a screen that shows all the passwords the
 * 				user has created and allows them to modify the list.
 * 				Also holds the list of existing passwords.
 * @author Adam Knox
 */


#ifndef MENU_H
#define MENU_H


// Included Libraries
#include <MAUI/EditBox.h>
#include <MAUI/Font.h>
#include <MAUI/Label.h>
#include <MAUI/Layout.h>
#include <MAUI/ListBox.h>
#include <MAUI/Screen.h>
#include <MAUtil/Vector.h>
#include <MAUtil/Moblet.h>


// Included Project Headers
#include "../Controller.h"
#include "../model/Password.h"
#include "../utilities/KineticListBox.h"
#include "CreatePassword.h"
#include "EditPassword.h"
#include "Theme.h"

// Need to include MA Headers if anything is pulled from the resources folder
#include "MAHeaders.h"


// Forward declarations


// Menu Window Class Declaration
class Menu :
	public MAUI::Screen
{
public:
	/**
	 * Constructor: Creates the graphical user interface
	 * @author Adam Knox
	 */
	Menu();

	/**
	 * Destructor
	 * @author Adam Knox
	 */
	virtual ~Menu();


	void Menu::loadPasswords();

	/**
	 * Checks which button is selected, and runs the action
	 * associated with the selected button.
	 * @author Adam Knox
	 * @return Outcome
	 */
	int runWidgetAction();

private:
	/**
	 * Listens for any key presses and does the associated action
	 * @author Adam Knox
	 * @param keyCode The code for the key pressed
	 */
	void keyPressEvent(const int keyCode);

	/**
	 * Listens for a press on the screen and runs proper action for the press
	 * @author Adam Knox
	 * @param point The position on the screen that is being pressed
	 */
	void pointerPressEvent(const MAPoint2d point);

	/**
	 * Listens for the release of a point on the screen and runs proper action for the release
	 * @author Adam Knox
	 * @param point The position on the screen that is being released
	 */
	void pointerReleaseEvent(const MAPoint2d point);

	// Pointers
	Theme* 							theme;				// Contains everything from fonts to skins to widget dimensions
	MAPoint2d						downPoint;			// The point at which the touch screen is held down
	MAUI::Layout* 					mainLayout;			// The main layout that holds the other widgets
	MAUI::Label* 					headerLaber;		// A text label that displays screen's name/title
	MAUI::Label* 					newPasswordButton;	// The New Password button
	MAUI::Label*					licenseButton;
	MAUI::ListBox* 					body;				// Scrolling area containing the password buttons
	MAUtil::Vector<Password>		passwordsList;		//
	MAUtil::Vector<MAUI::Label*> 	passwordWidgetsList;// List of the Password Buttons displayed on the screen
};

#endif
