#include "Input_PictureGrid.h"

#define GRID_WIDTH 4
#define GRID_HEIGHT 4

/**
 * Constructor: Builds the user interface
 * @author Adam Knox
 * @param ui the controller of this object
 */
Input_PictureGrid::Input_PictureGrid(){
	// Open the theme for setting up graphics, colours, and dimensioning of widgets
	theme = new Theme();

	// Size the buttons
	unsigned int iconXSpacing = (theme->bodyWidth)/GRID_WIDTH;
	unsigned int iconYSpacing = (theme->bodyHeight - theme->buttonHeight)/GRID_HEIGHT;
	unsigned int iconWidth = iconXSpacing/20;
	unsigned int iconHeight = iconYSpacing/20;
	unsigned int iconLength;	// Dimension of the square icon
	if (iconWidth < iconHeight) {
		iconLength = iconWidth;
	} else {
		iconLength = iconHeight;
	}

	// Build the layout
	// Create base layout object
	mainLayout = new MAUI::Layout(0, 0, theme->layoutWidth, theme->layoutHeight, NULL, 1, 3);
	mainLayout->setHorizontalAlignment(MAUI::Layout::HA_CENTER);
	if (theme->layoutSkin) {
		mainLayout->setSkin(theme->layoutSkin);
		mainLayout->setDrawBackground(true);
	} else if (theme->layoutBGColor > 0) {
		mainLayout->setBackgroundColor(theme->layoutBGColor);
		mainLayout->setDrawBackground(true);
	} else {
		mainLayout->setDrawBackground(false);
	}
	setMain(mainLayout);

	// Setup the screen's header label
	headerLabel = new MAUI::Label (0, 0,	theme->headerWidth, theme->headerHeight, mainLayout, "", theme->headerBGColor, theme->headerFont);
	headerLabel->setHorizontalAlignment(MAUI::Label::HA_CENTER);
	headerLabel->setVerticalAlignment(MAUI::Label::VA_CENTER);

	if (theme->headerSkin) {
		headerLabel->setSkin(theme->headerSkin);
		headerLabel->setDrawBackground(true);

	} else if (theme->headerBGColor != 0) {
		headerLabel->setBackgroundColor(theme->headerBGColor);
		headerLabel->setDrawBackground(true);

	} else {
		headerLabel->setDrawBackground(false);
	}


	// Body Spacer
	MAUI::Label* spacer = new MAUI::Label(0, 0, theme->bodyWidth, theme->bodyPadding, mainLayout);
	spacer->setDrawBackground(false);


	// Setup the body that holds the buttons to access the stored password objects
	body = new MAUI::ListBox(0, 0, theme->bodyWidth, theme->bodyHeight, mainLayout, MAUI::ListBox::LBO_VERTICAL, MAUI::ListBox::LBA_LINEAR, true);
	// Theme the scroll window
	if (theme->bodySkin) {
		body->setSkin(theme->bodySkin);
		body->setDrawBackground(true);
	} else if (theme->bodyBGColor > 0) {
		body->setBackgroundColor(theme->bodyBGColor);
		body->setDrawBackground(true);
	} else {
		body->setDrawBackground(false);
	}


	// Setup the grid that holds the sequence icons
	gridLayout = new MAUI::Layout(0, 0, theme->bodyWidth, theme->bodyHeight - theme->buttonHeight, body, GRID_WIDTH, GRID_HEIGHT);



	// Grid to hold the action buttons
	actionButtonGrid = new MAUI::Layout(0, 0, theme->bodyWidth, theme->buttonHeight, body, 2, 1);

	// Create the button that displays the license entry screen

	if (Settings::MULTIPLE_PASSWORDS) {
		secondaryButton = new MAUI::Label(0, 0,	theme->buttonWidth/2, theme->buttonHeight, actionButtonGrid, "Edit", theme->button2BGColor, theme->buttonFont);
	} else {
		secondaryButton = new MAUI::Label(0, 0,	theme->buttonWidth/2, theme->buttonHeight, actionButtonGrid, "License", theme->button2BGColor, theme->buttonFont);
	}
	secondaryButton->setHorizontalAlignment(MAUI::Label::HA_CENTER);
	secondaryButton->setVerticalAlignment(MAUI::Label::VA_CENTER);
	// Theme the license button
	if (theme->button2Skin) {
		secondaryButton->setSkin(theme->button2Skin);
		secondaryButton->setDrawBackground(true);

	} else if (theme->button2BGColor != 0) {
		secondaryButton->setBackgroundColor(theme->button2BGColor);
		secondaryButton->setDrawBackground(true);

	} else {
		secondaryButton->setDrawBackground(false);
	}

	// Create the button for running the password generation algorithm
	goButton = new MAUI::Label(0, 0,	theme->buttonWidth/2, theme->buttonHeight, actionButtonGrid, "GO", theme->button1BGColor, theme->buttonFont);
	goButton->setHorizontalAlignment(MAUI::Label::HA_CENTER);
	goButton->setVerticalAlignment(MAUI::Label::VA_CENTER);
	// Theme the go button
	if (theme->button1Skin) {
		goButton->setSkin(theme->button1Skin);
		goButton->setDrawBackground(true);

	} else if (theme->button1BGColor != 0) {
		goButton->setBackgroundColor(theme->button1BGColor);
		goButton->setDrawBackground(true);

	} else {
		goButton->setDrawBackground(false);
	}


	// Setup the grid
	for (int i = 0; i < GRID_HEIGHT; i++) {
		imageGrid.add(MAUtil::Vector<MAUI::Image*>());
		for (int j = 0; j < GRID_WIDTH; j++) {
			// Add the image to the row
			imageGrid[i].add(new MAUI::Image(0, 0,	iconXSpacing, iconYSpacing, gridLayout, false, false));
			imageGrid[i][j]->setDrawBackground(true);
			imageGrid[i][j]->setBackgroundColor(theme->gridBGColorUp);
		}
	}


	// Set images in grid
	imageGrid[0][0]->setResource(GRID_BUTTON_01);
	imageGrid[0][1]->setResource(GRID_BUTTON_02);
	imageGrid[0][2]->setResource(GRID_BUTTON_03);
	imageGrid[0][3]->setResource(GRID_BUTTON_04);
	imageGrid[1][0]->setResource(GRID_BUTTON_05);
	imageGrid[1][1]->setResource(GRID_BUTTON_06);
	imageGrid[1][2]->setResource(GRID_BUTTON_07);
	imageGrid[1][3]->setResource(GRID_BUTTON_08);
	imageGrid[2][0]->setResource(GRID_BUTTON_09);
	imageGrid[2][1]->setResource(GRID_BUTTON_10);
	imageGrid[2][2]->setResource(GRID_BUTTON_11);
	imageGrid[2][3]->setResource(GRID_BUTTON_12);
	imageGrid[3][0]->setResource(GRID_BUTTON_13);
	imageGrid[3][1]->setResource(GRID_BUTTON_14);
	imageGrid[3][2]->setResource(GRID_BUTTON_15);
	imageGrid[3][3]->setResource(GRID_BUTTON_16);

	// Only want to show this screen when it is used, not when it is created
	this->hide();

};





/**
 * Destructor
 * @author Adam Knox
 */
Input_PictureGrid::~Input_PictureGrid() {
	delete	theme;
	delete 	headerLabel;
	delete 	gridLayout;
	delete 	actionButtonGrid;
	delete 	goButton;
	delete 	secondaryButton;
	delete 	body;
	delete 	mainLayout;
}







void Input_PictureGrid::open(const Password local_password) {
	// Use the password given for creating the password
	password.clone(local_password);

	// Set the window heading
	headerLabel->setCaption(password.name);

	// Clear the the user's key
	iconSequence = MAUtil::Vector<int> ();

	// Reset the grid
	// Set the first image button to be selected randomly
	srand(maGetMilliSecondCount());
	selectedX = rand() % GRID_WIDTH;
	selectedY = rand() % GRID_HEIGHT;


	for (int i = 0; i < GRID_HEIGHT; i++) {
		for (int j = 0; j < GRID_WIDTH; j++) {
			imageGrid[i][j]->setBackgroundColor(theme->gridBGColorUp);
			imageGrid[i][j]->setSelected(false);
		}
	}
	imageGrid[selectedY][selectedX]->setBackgroundColor(theme->gridBGColorDown);
	imageGrid[selectedY][selectedX]->setSelected(true);
	goButton->setSelected(false);

	// Display the screen
	this->show();
}





/**
 * Runs commands for any key presses on the window's buttons
 * @author Adam Knox
 * @param keyCode the code associated with the key pressed
 */
void Input_PictureGrid::keyPressEvent(const int keyCode) {
	Controller ctrl;

	// Choose whichever button was pressed
	switch (keyCode) {
		// Return to home or close the program
		case MAK_BACK:
		case MAK_SOFTLEFT:
			if (Settings::MULTIPLE_PASSWORDS) {
				this->hide();
				iconSequence = MAUtil::Vector<int> ();
				ctrl.showScreen(HOME);
			} else {
				maExit(0);
			}
			break;

		// Move up on the grid
		case MAK_UP:

			// if on go button, then move into grid instead
			if (goButton->isSelected()) {
				gridLayout->setSelected(true);
				imageGrid[selectedY][selectedX]->setSelected(true);
				imageGrid[selectedY][selectedX]->setBackgroundColor(theme->gridBGColorDown);
				goButton->setSelected(false);
			}

			// Move up in the grid if not at top
			else if (selectedY > 0) {
				imageGrid[selectedY][selectedX]->setSelected(false);
				imageGrid[selectedY][selectedX]->setBackgroundColor(theme->gridBGColorUp);
				selectedY--;
				imageGrid[selectedY][selectedX]->setSelected(true);
				imageGrid[selectedY][selectedX]->setBackgroundColor(theme->gridBGColorDown);
			}
			break;

		// Move down on the grid
		case MAK_DOWN:
			// move down in the grid if above the bottom row
			if (selectedY < GRID_HEIGHT - 1) {
				imageGrid[selectedY][selectedX]->setSelected(false);
				imageGrid[selectedY][selectedX]->setBackgroundColor(theme->gridBGColorUp);
				selectedY++;
				imageGrid[selectedY][selectedX]->setSelected(true);
				imageGrid[selectedY][selectedX]->setBackgroundColor(theme->gridBGColorDown);
			}

			// If in the bottom row then move to the go button
			else if (selectedY == GRID_HEIGHT - 1) {
				imageGrid[selectedY][selectedX]->setSelected(false);
				imageGrid[selectedY][selectedX]->setBackgroundColor(theme->gridBGColorUp);
				gridLayout->setSelected(false);
				goButton->setSelected(true);
			}
			break;

		// Move left on the grid
		case MAK_LEFT:
			if ((!goButton->isSelected()) && (selectedX > 0)) {
				imageGrid[selectedY][selectedX]->setSelected(false);
				imageGrid[selectedY][selectedX]->setBackgroundColor(theme->gridBGColorUp);
				selectedX--;
				imageGrid[selectedY][selectedX]->setSelected(true);
				imageGrid[selectedY][selectedX]->setBackgroundColor(theme->gridBGColorDown);

			} else if (goButton->isSelected()) {
				goButton->setSelected(false);
				secondaryButton->setSelected(true);
			}
			break;

		// Move right on the grid
		case MAK_RIGHT:
			if ((!goButton->isSelected()) && (selectedX < GRID_WIDTH - 1)) {
				imageGrid[selectedY][selectedX]->setSelected(false);
				imageGrid[selectedY][selectedX]->setBackgroundColor(theme->gridBGColorUp);
				selectedX++;
				imageGrid[selectedY][selectedX]->setSelected(true);
				imageGrid[selectedY][selectedX]->setBackgroundColor(theme->gridBGColorDown);

			} else if (secondaryButton->isSelected()) {
				goButton->setSelected(true);
				secondaryButton->setSelected(false);
			}
			break;

		case MAK_FIRE:

			// Run the algorithm with the current sequence
			if (goButton->isSelected()) {
				// Tell the ui controller that this screen is done, and needs to run
				// a password generation
				this->hide();
				if (!ctrl.showKey(password, iconSequence)) {
					open(password);
				}
				iconSequence = MAUtil::Vector<int> ();
			}

			else if (secondaryButton->isSelected()) {
				secondaryButton->setSelected(false);
				hide();
				iconSequence = MAUtil::Vector<int> ();
				Controller ctrl;
				if (Settings::MULTIPLE_PASSWORDS) {
					ctrl.showScreen(EDIT_PASSWORD);
				} else {
					ctrl.showScreen(RENEW_LICENSE);
				}
			}

			// Add the selected image to the sequence or run the algorithm
			else {
				iconSequence.add( selectedY + (selectedX * 10) );
			}

			break;
	}
}





/**
 * Listens for a press on the screen and runs proper action for the press
 * @author Adam Knox
 * @param point The position on the screen that is being pressed
 */
void Input_PictureGrid::pointerPressEvent(const MAPoint2d point) {
	// Clear existing selection
	goButton->setSelected(false);
	for (int i = 0; i < GRID_HEIGHT; i++) {
		for (int j = 0; j < GRID_WIDTH; j++) {
			if (imageGrid[i][j]->contains(point.x, point.y)) {
				imageGrid[i][j]->setSelected(true);
				imageGrid[i][j]->setBackgroundColor(theme->gridBGColorDown);
			} else {
				imageGrid[i][j]->setSelected(false);
				imageGrid[i][j]->setBackgroundColor(theme->gridBGColorUp);
			}
		}
	}

	// Make New Selection
	if (goButton->contains(point.x, point.y)){
		goButton->setSelected(true);
	} else if (secondaryButton->contains(point.x, point.y)) {
		secondaryButton->setSelected(true);
	}
}





/**
 * Listens for the release of a point on the screen and runs proper action for the release
 * @author Adam Knox
 * @param point The position on the screen that is being released
 */
void Input_PictureGrid::pointerReleaseEvent(const MAPoint2d point) {
	// Add the selected image to the sequence
	for (int i = 0; i < GRID_HEIGHT; i++) {
		for (int j = 0; j < GRID_WIDTH; j++) {
			if (imageGrid[i][j]->isSelected()) {
				iconSequence.add(i+j*10);
			}
		}
	}

	// Run the algorithm with the current sequence
	if (goButton->isSelected()) {
		goButton->setSelected(false);
		// Tell the ui controller that this screen is done, and needs to run
		// a password generation
		hide();
		Controller ctrl;
		if (!ctrl.showKey(password, iconSequence)) {
			open(password);
		}
		iconSequence = MAUtil::Vector<int> ();

	} else if (secondaryButton->isSelected()) {
		secondaryButton->setSelected(false);
		hide();
		iconSequence = MAUtil::Vector<int> ();
		Controller ctrl;
		if (Settings::MULTIPLE_PASSWORDS) {
			ctrl.showScreen(EDIT_PASSWORD, password);
		} else {
			ctrl.showScreen(RENEW_LICENSE);
		}
	}
}

