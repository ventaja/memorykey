/**
 * The screen used for setting up a new password
 * @author Adam Knox
 */


#include "CreatePassword.h"

/**
 * Constructor: Builds the basic layout, then calls the proper function to build the customized part of the layout.
 * @author Adam Knox
 * @param ui The address to the controller of this screen
 * @param window The type of window to display
 */
CreatePassword::CreatePassword() {


	//Initialise the class to pull info for setting themes from
	theme = new Theme();


	// Build the UI's Layout
	mainLayout = new MAUI::Layout(0, 0, theme->layoutWidth, theme->layoutHeight, NULL, 1, 3);
	mainLayout->setHorizontalAlignment(MAUI::Layout::HA_CENTER);

	// Theme the layout
	if (theme->layoutSkin) {
		mainLayout->setSkin(theme->layoutSkin);
		mainLayout->setDrawBackground(true);
	} else if (theme->layoutBGColor != 0) {
		mainLayout->setBackgroundColor(theme->layoutBGColor);
		mainLayout->setDrawBackground(true);
	} else {
		mainLayout->setDrawBackground(false);
	}
	setMain(mainLayout);


	// Setup the screen's header label
	headerLabel = new MAUI::Label(0, 0,	theme->headerWidth, theme->headerHeight, mainLayout, "", theme->headerBGColor, theme->headerFont);
	// Theme the header
	if (theme->headerSkin) {
		headerLabel->setSkin(theme->headerSkin);
		headerLabel->setDrawBackground(true);
	} else if (theme->headerBGColor > 0) {
		headerLabel->setBackgroundColor(theme->headerBGColor);
		headerLabel->setDrawBackground(true);
	} else {
		headerLabel->setDrawBackground(true);
	}
	headerLabel->setHorizontalAlignment(MAUI::Label::HA_CENTER);
	headerLabel->setVerticalAlignment(MAUI::Label::VA_CENTER);
	headerLabel->setCaption("Create Password");


	// Body Spacer
	MAUI::Label* spacer = new MAUI::Label(0, 0, theme->bodyWidth, theme->bodyPadding, mainLayout);
	spacer->setDrawBackground(false);


	// Setup the scrolling window
	body = new MAUI::KineticListBox(0, 0, theme->bodyWidth, theme->bodyHeight - theme->keyboardHeight, mainLayout, MAUI::KineticListBox::LBO_VERTICAL, MAUI::KineticListBox::LBA_LINEAR, false);
	// Theme the scroll window
	if (theme->bodySkin) {
		body->setSkin(theme->bodySkin);
		body->setDrawBackground(true);
	} else if (theme->bodyBGColor > 0) {
		body->setBackgroundColor(theme->bodyBGColor);
		body->setDrawBackground(true);
	} else {
		body->setDrawBackground(false);
	}





	// Add the parameter fields and containers ***MUST BE ADDED SO THE LIST ORDER CAN BE READ OUT IN THE ORDER PASSWORD TAKES THE PARAMS DURING DECLARATION
	paramContainersList.add(new MAUI::Label(0, 0, theme->ebContainerWidth, theme->ebContainerHeight, body, "", theme->ebContainerBGColor, theme->ebContainerFont));
	paramLabelsList.add(new MAUI::Label(theme->ebLabelXOffset, theme->ebLabelYOffset, theme->ebLabelWidth, theme->ebLabelHeight, paramContainersList[0], "Name", theme->ebLabelBGColor, theme->ebLabelFont));
	paramFieldsList.add(new MAUI::EditBox(theme->editBoxXOffset, theme->editBoxYOffset, theme->editBoxWidth, theme->editBoxHeight, paramContainersList[0], "", theme->editBoxBGColor, theme->editBoxFont, true, false, 60, MAUI::EditBox::IM_QWERTY));

	paramContainersList.add(new MAUI::Label(0, 0, theme->ebContainerWidth, theme->ebContainerHeight, body, "", theme->ebContainerBGColor, theme->ebContainerFont));
	paramLabelsList.add(new MAUI::Label(theme->ebLabelXOffset, theme->ebLabelYOffset, theme->ebLabelWidth, theme->ebLabelHeight, paramContainersList[1], "# Special", theme->ebLabelBGColor, theme->ebLabelFont));
	paramFieldsList.add(new MAUI::EditBox(theme->editBoxXOffset, theme->editBoxYOffset, theme->editBoxWidth, theme->editBoxHeight, paramContainersList[1], "", theme->editBoxBGColor, theme->editBoxFont, true, false, 60, MAUI::EditBox::IM_NUMBERS));

	paramContainersList.add(new MAUI::Label(0, 0, theme->ebContainerWidth, theme->ebContainerHeight, body, "", theme->ebContainerBGColor, theme->ebContainerFont));
	paramLabelsList.add(new MAUI::Label(theme->ebLabelXOffset, theme->ebLabelYOffset, theme->ebLabelWidth, theme->ebLabelHeight, paramContainersList[2], "# Numbers", theme->ebLabelBGColor, theme->ebLabelFont));
	paramFieldsList.add(new MAUI::EditBox(theme->editBoxXOffset, theme->editBoxYOffset, theme->editBoxWidth, theme->editBoxHeight, paramContainersList[2], "", theme->editBoxBGColor, theme->editBoxFont, true, false, 60, MAUI::EditBox::IM_NUMBERS));

	paramContainersList.add(new MAUI::Label(0, 0, theme->ebContainerWidth, theme->ebContainerHeight, body, "", theme->ebContainerBGColor, theme->ebContainerFont));
	paramLabelsList.add(new MAUI::Label(theme->ebLabelXOffset, theme->ebLabelYOffset, theme->ebLabelWidth, theme->ebLabelHeight, paramContainersList[3], "# Upper Case", theme->ebLabelBGColor, theme->ebLabelFont));
	paramFieldsList.add(new MAUI::EditBox(theme->editBoxXOffset, theme->editBoxYOffset, theme->editBoxWidth, theme->editBoxHeight, paramContainersList[3], "", theme->editBoxBGColor, theme->editBoxFont, true, false, 60, MAUI::EditBox::IM_NUMBERS));

	paramContainersList.add(new MAUI::Label(0, 0, theme->ebContainerWidth, theme->ebContainerHeight, body, "", theme->ebContainerBGColor, theme->ebContainerFont));
	paramLabelsList.add(new MAUI::Label(theme->ebLabelXOffset, theme->ebLabelYOffset, theme->ebLabelWidth, theme->ebLabelHeight, paramContainersList[4], "# Lower Case", theme->ebLabelBGColor, theme->ebLabelFont));
	paramFieldsList.add(new MAUI::EditBox(theme->editBoxXOffset, theme->editBoxYOffset, theme->editBoxWidth, theme->editBoxHeight, paramContainersList[4], "", theme->editBoxBGColor, theme->editBoxFont, true, false, 60, MAUI::EditBox::IM_NUMBERS));

	paramContainersList.add(new MAUI::Label(0, 0, theme->ebContainerWidth, theme->ebContainerHeight, body, "", theme->ebContainerBGColor, theme->ebContainerFont));
	paramLabelsList.add(new MAUI::Label(theme->ebLabelXOffset, theme->ebLabelYOffset, theme->ebLabelWidth, theme->ebLabelHeight, paramContainersList[5], "Max Password Length", theme->ebLabelBGColor, theme->ebLabelFont));
	paramFieldsList.add(new MAUI::EditBox(theme->editBoxXOffset, theme->editBoxYOffset, theme->editBoxWidth, theme->editBoxHeight, paramContainersList[5], "", theme->editBoxBGColor, theme->editBoxFont, true, false, 60, MAUI::EditBox::IM_NUMBERS));


	// Set up the fields
	for (int i = 0; i < paramFieldsList.size(); i++) {
		// Setup the editbox
		paramFieldsList[i]->activate();
		paramFieldsList[i]->setSelected(false);
		// Theme editbox
		if (theme->editBoxSkin) {
			paramFieldsList[i]->setSkin(theme->editBoxSkin);
			paramFieldsList[i]->setDrawBackground(true);
		} else if (theme->editBoxBGColor > 0) {
			paramFieldsList[i]->setBackgroundColor(theme->editBoxBGColor);
			paramFieldsList[i]->setDrawBackground(true);
		} else {
			paramFieldsList[i]->setDrawBackground(false);
		}


		// Theme field container label
		if (theme->ebLabelSkin) {
			paramLabelsList[i]->setSkin(theme->ebLabelSkin);
			paramLabelsList[i]->setDrawBackground(true);
		} else if (theme->ebLabelBGColor > 0) {
			paramLabelsList[i]->setBackgroundColor(theme->ebLabelBGColor);
			paramLabelsList[i]->setDrawBackground(true);
		} else {
			paramLabelsList[i]->setDrawBackground(false);
		}


		// Theme Field Container label
		if (theme->ebContainerSkin) {
			paramContainersList[i]->setSkin(theme->ebContainerSkin);
			paramContainersList[i]->setDrawBackground(true);
		} else if (theme->ebContainerBGColor > 0) {
			paramContainersList[i]->setBackgroundColor(theme->ebContainerBGColor);
			paramContainersList[i]->setDrawBackground(true);
		} else {
			paramContainersList[i]->setDrawBackground(false);
		}

	}


	// Build the create password button
	createButton 	= new MAUI::Label		(0, 0, theme->buttonWidth, theme->buttonHeight, body, "Create Password", theme->button1BGColor, theme->buttonFont);
	createButton	->setHorizontalAlignment(MAUI::Label::HA_CENTER);
	createButton	->setVerticalAlignment	(MAUI::Label::VA_CENTER);
	// Theme button
	if (theme->button1Skin) {
		createButton->setSkin(theme->button1Skin);
		createButton->setDrawBackground(true);
	} else if (theme->button1BGColor > 0) {
		createButton->setBackgroundColor(theme->button1BGColor);
		createButton->setDrawBackground(true);
	} else {
		createButton->setDrawBackground(false);
	}
}





/**
 * Destructor
 * @author Adam Knox
 */
CreatePassword::~CreatePassword() {
	paramFieldsList.clear();
	paramLabelsList.clear();
	paramContainersList.clear();

	delete headerLabel;
	delete mainLayout;
	delete body;
	delete createButton;
	delete theme;
}





void CreatePassword::open() {
	// Populate fields with data from the given password
	paramFieldsList[0]->setCaption("");
	paramFieldsList[1]->setCaption("2");
	paramFieldsList[2]->setCaption("2");
	paramFieldsList[3]->setCaption("2");
	paramFieldsList[4]->setCaption("2");
	paramFieldsList[5]->setCaption("10");

	// Select the first field
	bodyIndex = 0;
	paramFieldsList[bodyIndex]->setSelected(true);
	paramFieldsList[bodyIndex]->moveCursorHorizontal(paramFieldsList[bodyIndex]->getCaption().length(),false);
	body->setSelectedIndex(bodyIndex);
	maShowVirtualKeyboard();
	this->show();
}










/**
 * Runs the action for the selected widget
 * @author Adam Knox
 * @return Completion Status
 */
int CreatePassword::runWidgetAction() {
	// todo: Make sure only valid characters have been entered into the editboxes

	if (createButton->isSelected()) {
		// Create the new password
		Password* newPassword = new Password(	paramFieldsList[0]->getText().c_str(),
												"",
												PICTURE_GRID,
												0,
												Convert::toInt(paramFieldsList[1]->getText()),
												Convert::toInt(paramFieldsList[2]->getText()),
												Convert::toInt(paramFieldsList[3]->getText()),
												Convert::toInt(paramFieldsList[4]->getText()),
												Convert::toInt(paramFieldsList[5]->getText()));

		// Store the new password
		Controller ctrl;
		if (ctrl.createPassword(*newPassword)) {
			delete newPassword;
			this->hide();
			ctrl.showScreen(HOME);
		}

		else {
			//todo: notify user of error
		}

	}

}





/**
 * Listens for any key presses and does the associated action
 * @author Adam Knox
 * @param keyCode The code for the key pressed
 */
void CreatePassword::keyPressEvent(const int keyCode, const int nativeCode) {
	Controller ctrl;

	switch (keyCode) {
		case MAK_BACK:
		case MAK_SOFTLEFT:
			// Hide this screen and go back to the one that called it
			this->hide();
			ctrl.showScreen(HOME);
			break;

		case MAK_UP:
			// Moving within the edit boxes
			if (bodyIndex < NUM_FIELDS) {
				paramFieldsList[bodyIndex]->setSelected(false);
				if (bodyIndex > 0) {bodyIndex--;}
				paramFieldsList[bodyIndex]->setSelected(true);
				paramFieldsList[bodyIndex]->moveCursorHorizontal(paramFieldsList[bodyIndex]->getCaption().length(),false);
				maShowVirtualKeyboard();

			// Moving from edit button to edit boxes
			} else if (bodyIndex == NUM_FIELDS) {
				bodyIndex--;
				createButton->setSelected(false);
				paramFieldsList[bodyIndex]->setSelected(true);
				paramFieldsList[bodyIndex]->moveCursorHorizontal(paramFieldsList[bodyIndex]->getCaption().length(),false);
				maShowVirtualKeyboard();
			}
			body->setSelectedIndex(bodyIndex);

			break;

		case 10:
			// if on the submit button, then submit. otherwise move to next item
			if (createButton->isSelected()) {
				runWidgetAction();
			} else {
				keyPressEvent(MAK_DOWN, 0);
			}
			break;

		case MAK_DOWN:
			// Moving within the edit boxes
			if (bodyIndex < NUM_FIELDS - 1) {
				paramFieldsList[bodyIndex]->setSelected(false);
				bodyIndex++;
				paramFieldsList[bodyIndex]->setSelected(true);
				paramFieldsList[bodyIndex]->moveCursorHorizontal(paramFieldsList[bodyIndex]->getCaption().length(),false);
				maShowVirtualKeyboard();

			// Moving from edit boxes to edit button
			} else if (bodyIndex == NUM_FIELDS - 1) {
				paramFieldsList[bodyIndex]->setSelected(false);
				bodyIndex++;
				createButton->setSelected(false);
			}
			body->setSelectedIndex(bodyIndex);
			break;

		case MAK_FIRE:
			if (createButton->isSelected()) {
				// Run the selected widget
				runWidgetAction();
			} else {
				keyPressEvent(MAK_DOWN, 0);
			}
			break;
/*
		case 12:
		case 127:
			// Try to delete the last character in the edit box
			if (selectedEditBox != NULL) {
				MAUI::EditBox* eb = (MAUI::EditBox*)selectedEditBox;
				MAUtil::String newCaption = eb->getCaption();
				if (newCaption.length() > 0) {
					eb->setCaption(newCaption.substr(0, newCaption.length() - 1));
				}
			}
*/
		default:
			// Try to put the character in an editbox
			if (bodyIndex < NUM_FIELDS) {
				MAUtil::String character = Convert::asciiCodeToString(keyCode);
				MAUtil::String newCaption = paramFieldsList[bodyIndex]->getCaption();
				newCaption.append(character.c_str(), character.length());
				paramFieldsList[bodyIndex]->setCaption(newCaption);
				paramFieldsList[bodyIndex]->moveCursorHorizontal(1, false);
			}
			break;
	}
}





/**
 * Listens for a press on the screen and runs proper action for the press
 * @author Adam Knox
 * @param point The position on the screen that is being pressed
 */
void CreatePassword::pointerPressEvent(const MAPoint2d point) {
	// Store the current cursor location
	downPoint = point;
}





/**
 * Listens for the release of a point on the screen and runs proper action for the release
 * @author Adam Knox
 * @param point The position on the screen that is being released
 */
void CreatePassword::pointerReleaseEvent(const MAPoint2d point) {
	int yOffset = body->getYOffset() >> 16;

	// Only want to select an item if it is visible on the screen (i.e. within the body object's bounds)
	if (body->contains(point.x, point.y) && body->contains(downPoint.x, downPoint.y)) {
		// Check if an editbox should be selected
		for (int i = 0; i < paramFieldsList.size(); i++) {

			// Add the labels to the widget listener so selection switches to their first child widget
			if (paramFieldsList[i]->contains(point.x, point.y - yOffset) && paramFieldsList[i]->contains(point.x, point.y - yOffset)) {
				// un select the currently selected editbox
				paramFieldsList[bodyIndex]->setSelected(false);

				// reassign the selected editbox and select it
				bodyIndex = i;
				paramFieldsList[i]->setSelected(true);
				paramFieldsList[i]->moveCursorHorizontal(paramFieldsList[i]->getCaption().length(),false);
				maShowVirtualKeyboard();
			}

		}

		// Only run an action if the pointer has not moved since being pressed down
		if (createButton->contains(point.x,point.y - yOffset) && createButton->contains(downPoint.x, downPoint.y - yOffset)) {
			createButton->setSelected();
			runWidgetAction();
		}
	}
}
