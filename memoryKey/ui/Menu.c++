/**
 * description: Builds a screen that shows all the passwords the
 * 				user has created and allows them to modify the list.
 * 				Also holds the list of existing passwords.
 * @author Adam Knox
 */


#include "Menu.h"


/**
 * Constructor
 * @author Adam Knox
 */
Menu::Menu() {
	// Open the theme used to skin the program
	theme = new Theme();


	// Build the main layout
	mainLayout = new MAUI::Layout(0, 0, theme->layoutWidth, theme->layoutHeight, NULL, 1, 3);
	mainLayout->setHorizontalAlignment(MAUI::Layout::HA_CENTER);
	// theme layout
	if (theme->layoutSkin) {
		mainLayout->setSkin(theme->layoutSkin);
		mainLayout->setDrawBackground(true);
	} else if (theme->layoutBGColor > 0) {
		mainLayout->setBackgroundColor(theme->layoutBGColor);
		mainLayout->setDrawBackground(true);
	} else {
		mainLayout->setDrawBackground(false);
	}
	setMain(mainLayout);


	// Setup the screen's header label
	headerLaber = new MAUI::Label (0, 0, theme->headerWidth, theme->headerHeight, mainLayout, "Menu", theme->headerBGColor, theme->headerFont);
	headerLaber->setHorizontalAlignment(MAUI::Label::HA_CENTER);
	headerLaber->setVerticalAlignment(MAUI::Label::VA_CENTER);
	// theme header
	if (theme->headerSkin) {
		headerLaber->setSkin(theme->headerSkin);
		headerLaber->setDrawBackground(true);
	} else if (theme->headerBGColor != 0) {
		headerLaber->setBackgroundColor(theme->headerBGColor);
		headerLaber->setDrawBackground(true);
	} else {
		headerLaber->setDrawBackground(false);
	}

	// Body Spacer
	MAUI::Label* spacer = new MAUI::Label(0, 0, theme->bodyWidth, theme->bodyPadding, mainLayout);
	spacer->setDrawBackground(false);

	// Setup the scrolling pane that holds the buttons to access the stored password objects
	body = new MAUI::ListBox(0, 0, theme->bodyWidth, theme->bodyHeight, mainLayout, MAUI::ListBox::LBO_VERTICAL, MAUI::ListBox::LBA_LINEAR, false);
	// Theme the scroll window
	if (theme->bodySkin) {
		body->setSkin(theme->bodySkin);
		body->setDrawBackground(true);
	} else if (theme->bodyBGColor > 0) {
		body->setBackgroundColor(theme->bodyBGColor);
		body->setDrawBackground(true);
	} else {
		body->setDrawBackground(false);
	}

	// Display the screen changes
	maUpdateScreen();
}






/**
 * Destructor
 * @author Adam Knox
 */
Menu::~Menu() {
	// Delete all the heap used by just the menu
	delete theme;
	delete headerLaber;
	delete body;
	delete newPasswordButton;
	delete mainLayout;
	delete licenseButton;
}





void Menu::loadPasswords() {
	// Clear any existing password buttons
	body->clear();

	// Get list of password names
	passwordsList = MODEL->getPasswords();

	// Clear the password widget list
	passwordWidgetsList.clear();

	// Reverse through the list so passwordsList andpasswordWidgetsList line up
	for (int i = 0; i < passwordsList.size(); i++) {
		MAUI::Label* wPasswordObj;

		if (i % 2 > 0) {
			// Create the label button
			wPasswordObj = new MAUI::Label(
					0, 						// Left.
					0, 						// Top (will be set by the layout).
					theme->buttonWidth, 	// Width.
					theme->buttonHeight,	// Height.
					body, 					// Parent widget.
					(passwordsList)[i].getName(), 	// Button text.
					theme->button2BGColor,	// Background color
					theme->buttonFont);		// Font.

			// theme button
			if (theme->button2Skin) {
				wPasswordObj->setSkin(theme->button2Skin);
				wPasswordObj->setDrawBackground(true);
			} else if (theme->button2BGColor != 0) {
				wPasswordObj->setBackgroundColor(theme->button2BGColor);
				wPasswordObj->setDrawBackground(true);
			} else {
				wPasswordObj->setDrawBackground(false);
			}

		} else {
			// Create the label button
			wPasswordObj = new MAUI::Label(
					0, 						// Left.
					0, 						// Top (will be set by the layout).
					theme->buttonWidth, 	// Width.
					theme->buttonHeight,		// Height.
					body, 					// Parent widget.
					(passwordsList)[i].getName(), 	// Button text.
					theme->button3BGColor,	// Background color
					theme->buttonFont);		// Font.

			// theme button
			if (theme->button3Skin) {
				wPasswordObj->setSkin(theme->button3Skin);
				wPasswordObj->setDrawBackground(true);
			} else if (theme->button3BGColor != 0) {
				wPasswordObj->setBackgroundColor(theme->button3BGColor);
				wPasswordObj->setDrawBackground(true);
			} else {
				wPasswordObj->setDrawBackground(false);
			}
		}

		// Setup the new button
		wPasswordObj->setHorizontalAlignment(MAUI::Label::HA_CENTER);
		wPasswordObj->setVerticalAlignment(MAUI::Label::VA_CENTER);

		// Add the button to the list of displayed buttons
		passwordWidgetsList.add(wPasswordObj);
	}



	// Setup the screen's new password button
	newPasswordButton = new MAUI::Label(0, 0, theme->buttonWidth, theme->buttonHeight, body, "Create Password", theme->button1BGColor, theme->buttonFont);
	newPasswordButton->setHorizontalAlignment(MAUI::Label::HA_CENTER);
	newPasswordButton->setVerticalAlignment(MAUI::Label::VA_CENTER);
	// Theme the scroll window
	if (theme->button1Skin) {
		newPasswordButton->setSkin(theme->button1Skin);
		newPasswordButton->setDrawBackground(true);
	} else if (theme->button1BGColor > 0) {
		newPasswordButton->setBackgroundColor(theme->button1BGColor);
		newPasswordButton->setDrawBackground(true);
	} else {
		newPasswordButton->setDrawBackground(false);
	}



	// Create the button that displays the license entry screen
	licenseButton = new MAUI::Label(0, 0,	theme->buttonWidth, theme->buttonHeight, body, "Set License", theme->button1BGColor, theme->buttonFont);
	licenseButton->setHorizontalAlignment(MAUI::Label::HA_CENTER);
	licenseButton->setVerticalAlignment(MAUI::Label::VA_CENTER);
	// Theme the license button
	if (theme->button1Skin) {
		licenseButton->setSkin(theme->button1Skin);
		licenseButton->setDrawBackground(true);

	} else if (theme->button1BGColor != 0) {
		licenseButton->setBackgroundColor(theme->button1BGColor);
		licenseButton->setDrawBackground(true);

	} else {
		licenseButton->setDrawBackground(false);
	}





	body->setSelectedIndex(0);

	// Display the screen changes
	maUpdateScreen();
}





/**
 * Checks which button is selected, and runs the action
 * associated with the selected button.
 * @author Adam Knox
 * @return Outcome
 */
int Menu::runWidgetAction() {
	Controller ctrl;

	// Open create password window if new password button is pressed
	if (newPasswordButton->isSelected()) {
		// CreatePassword* sCreatePassword = new CreatePassword(this->currentScreen);
		this->hide();
		ctrl.showScreen(CREATE_PASSWORD);
	} else if (licenseButton->isSelected()) {
		this->hide();
		ctrl.showScreen(RENEW_LICENSE);
	}

	// Check through each password button to see if one is selected,
	// if one is, then pass the password object to the input screen.
	else {
		for (int i = 0; i < passwordWidgetsList.size(); i++) {
			if (passwordWidgetsList[i]->isSelected()) {

				// Bring up the password input screen
				this->hide();
				ctrl.showScreen(passwordsList[i]);
			}
		}
	}
}





/**
 * Listens for any key presses and does the associated action
 * @author Adam Knox
 * @param keyCode The code for the key pressed
 */
void Menu::keyPressEvent(const int keyCode) {
	// Do the operation for the button that was pressed
	switch (keyCode) {
		case MAK_BACK:
		case MAK_SOFTLEFT:
			//Close the program
			maExit(0);
			break;

		case MAK_UP:
			body->selectPreviousItem();
			break;

		case MAK_DOWN:
			body->selectNextItem();
			break;

		case MAK_FIRE:
			// Run the action for the widget that was pressed
			runWidgetAction();
			break;
	}
}





/**
 * Listens for a press on the screen and runs proper action for the press
 * @author Adam Knox
 * @param point The position on the screen that is being pressed
 */

void Menu::pointerPressEvent(const MAPoint2d point) {
	// Select a button if currently on top of one
	for (int i = 0; i < passwordWidgetsList.size(); i++) {
		passwordWidgetsList[i]->setSelected(false);
		if (passwordWidgetsList[i]->contains(point.x, point.y)) {
			passwordWidgetsList[i]->setSelected(true);
		}
	}

	newPasswordButton->setSelected(false);
	if (newPasswordButton->contains(point.x, point.y)) {
		newPasswordButton->setSelected(true);
	}

	licenseButton->setSelected(false);
	if (licenseButton->contains(point.x, point.y)) {
		licenseButton->setSelected(true);
	}


	// Store the current cursor location
	downPoint = point;
}





/**
 * Listens for the release of a point on the screen and runs proper action for the release
 * @author Adam Knox
 * @param point The position on the screen that is being released
 */

void Menu::pointerReleaseEvent(const MAPoint2d point) {
	// Only run an action if the pointer has not moved since being pressed down
	if (downPoint.x == point.x && downPoint.y == point.y) {
		for (int i = 0; i < passwordWidgetsList.size(); i++) {
			// if release occurs over top of the button then run its operation
			if (passwordWidgetsList[i]->isSelected()) {
				runWidgetAction();
			}
		}

		if (newPasswordButton->isSelected() || licenseButton->isSelected()) {
			runWidgetAction();
		}

	}
}

