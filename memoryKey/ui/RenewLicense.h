/**
 * The screen used for setting up a new password
 * @author Adam Knox
 */


#ifndef RENEW_LICENSE_H
#define RENEW_LICENSE_H


// Included Libraries
// #include <conprint.h>
#include <MAUI/Screen.h>
#include <MAUI/Label.h>
#include <MAUI/Layout.h>
#include <MAUI/EditBox.h>
#include <MAUI/ListBox.h>
#include <ma.h>


// Included Project Headers
#include "Theme.h"
#include "../Controller.h"


// Set Password Window Class Declaration
class RenewLicense :
	public MAUI::Screen
{
public:
	RenewLicense();

	virtual ~RenewLicense();

	void open();

private:
	void applyLicense();

	/**
	 * Listens for any key presses and does the associated action
	 * @author Adam Knox
	 * @param keyCode The code for the key pressed
	 */
	void keyPressEvent(int keyCode, int nativeCode);

	/**
	 * Listens for a press on the screen and runs proper action for the press
	 * @author Adam Knox
	 * @param point The position on the screen that is being pressed
	 */
	void pointerPressEvent(const MAPoint2d point);

	/**
	 * Listens for the release of a point on the screen and runs proper action for the release
	 * @author Adam Knox
	 * @param point The position on the screen that is being released
	 */
	void pointerReleaseEvent(const MAPoint2d point);

	// Heap Pointers
	MAUI::Label* 					headerLabel;		// A text label that displays instructions
	MAUI::Label* 					licenseFieldLabel;	// A text label that displays instructions
	MAUI::EditBox*					licenseField;
	MAUI::Label* 					applyButton;		// Create Password Button
	MAUI::Label* 					cancelButton;		// Cancel Button
	MAUI::Layout* 					mainLayout;			// The main layout that holds the other widgets
	MAUI::ListBox* 					body;				// Scrolling area containing the labels and text boxes
	Theme* 							theme;				// The pointer to theme information
	MAUI::Layout*					actionButtonGrid;
	MAUI::Label*					spacer;
	MAUI::Label*					licenseFieldContainer;

	// Stack Data
	MAPoint2d pt;
};

#endif
