/**
 * Displays to the screen the key created by
 * the algorithm using the password constraints
 * @author Adam Knox
 */


#include "ViewKey.h"


/**
 * Constructor. Builds the screen for viewing the key
 * @author Adam Knox
 * @param ui the object handling this screen
 */
ViewKey::ViewKey() {
	// Open the theme used to skin the program
	theme = new Theme();


	// Build main layout
	mainLayout = new MAUI::Layout(0, 0, theme->layoutWidth, theme->layoutHeight, NULL, 1, 3);
	mainLayout->setHorizontalAlignment(MAUI::Layout::HA_CENTER);
	// Theme the layout
	if (theme->layoutSkin) {
		mainLayout->setSkin(theme->layoutSkin);
		mainLayout->setDrawBackground(true);
	} else if (theme->layoutBGColor > 0) {
		mainLayout->setBackgroundColor(theme->layoutBGColor);
		mainLayout->setDrawBackground(true);
	} else {
		mainLayout->setDrawBackground(false);
	}
	setMain(mainLayout);


	// Setup the header label
	headerLabel = new MAUI::Label (0, 0, theme->headerWidth, theme->headerHeight, mainLayout, "Your Key", theme->headerBGColor, theme->headerFont);
	headerLabel->setHorizontalAlignment(MAUI::Label::HA_CENTER);
	headerLabel->setVerticalAlignment(MAUI::Label::VA_CENTER);
	// Theme the header
	if (theme->headerSkin) {
		headerLabel->setSkin(theme->headerSkin);
		headerLabel->setDrawBackground(true);
	} else if (theme->headerBGColor != 0) {
		headerLabel->setBackgroundColor(theme->headerBGColor);
		headerLabel->setDrawBackground(true);
	} else {
		headerLabel->setDrawBackground(false);
	}

	// Body Spacer
	MAUI::Label* spacer = new MAUI::Label(0, 0, theme->bodyWidth, theme->bodyPadding, mainLayout);
	spacer->setDrawBackground(false);

	// Setup Body for key display box and done button
	body = new MAUI::ListBox(0, 0, theme->bodyWidth, theme->bodyHeight, mainLayout, MAUI::ListBox::LBO_VERTICAL, MAUI::ListBox::LBA_LINEAR, true);
		// Theme the scroll window
		if (theme->bodySkin) {
			body->setSkin(theme->bodySkin);
			body->setDrawBackground(true);
		} else if (theme->bodyBGColor > 0) {
			body->setBackgroundColor(theme->bodyBGColor);
			body->setDrawBackground(true);
		} else {
			body->setDrawBackground(false);
		}

	// Setup the password display label
	keyLabel = new MAUI::Label (0, 0, theme->displayKeyWidth, theme->displayKeyHeight, body, "", theme->displayKeyBGColor, theme->displayKeyFont);
	keyLabel->setHorizontalAlignment(MAUI::Label::HA_CENTER);
	keyLabel->setVerticalAlignment(MAUI::Label::VA_CENTER);
	// Theme the key label
	if (theme->displayKeySkin) {
		keyLabel->setSkin(theme->displayKeySkin);
		keyLabel->setDrawBackground(true);
	} else if (theme->displayKeyBGColor != 0) {
		keyLabel->setBackgroundColor(theme->displayKeyBGColor);
		keyLabel->setDrawBackground(true);
	} else {
		keyLabel->setDrawBackground(false);
	}


	// Setup the button to return to the main menu
	doneButton = new MAUI::Label(0, 0,theme->buttonWidth, theme->buttonHeight, body, "Done", theme->button1BGColor, theme->buttonFont);
	doneButton->setHorizontalAlignment(MAUI::Label::HA_CENTER);
	doneButton->setVerticalAlignment(MAUI::Label::VA_CENTER);
	// Theme done button
	if (theme->button1Skin) {
		doneButton->setSkin(theme->button1Skin);
		doneButton->setDrawBackground(true);
	} else if (theme->button1BGColor > 0) {
		doneButton->setBackgroundColor(theme->button1BGColor);
		doneButton->setDrawBackground(true);
	} else {
		doneButton->setDrawBackground(false);
	}
	doneButton->setSelected(true);
};





/**
 * Destructor. Gets rid of screen and its components
 * @author Adam Knox
 */
ViewKey::~ViewKey() {
	delete headerLabel;
	delete keyLabel;
	delete doneButton;
	delete theme;
	delete body;
	delete mainLayout;
}





void ViewKey::showKey(MAUtil::String local_key) {
	keyLabel->setCaption(local_key.c_str());
	local_key.clear();
}





/**
 * This method is called when a key is pressed.
 * @author Adam Knox
 * @param keyCode the code assignment to the key pressed
 *
 */
void ViewKey::keyPressEvent(const int keyCode) {
	Controller ctrl;

	switch (keyCode) {
		// Return to main menu
		case MAK_BACK:
		case MAK_SOFTLEFT:
			goHome();
			break;

		// Return to main menu if done button is pressed
		case MAK_FIRE:
			if (doneButton->isSelected()) {
				goHome();
			}
			break;
	}
}





/**
 * Listens for a press on the screen and runs proper action for the press
 * @author Adam Knox
 * @param point The position on the screen that is being pressed
 */
void ViewKey::pointerPressEvent(const MAPoint2d point) {
	// Select return button if pressing on it
	if (doneButton->contains(point.x, point.y)){
		doneButton->setSelected(true);
	}
}





/**
 * Listens for the release of a point on the screen and runs proper action for the release
 * @author Adam Knox
 * @param point The position on the screen that is being released
 */
void ViewKey::pointerReleaseEvent(const MAPoint2d point) {
	// Return to main menu
	if (doneButton->isSelected()) {
		goHome();
	}
}

void ViewKey::goHome() {
	Controller ctrl;
	this->hide();
	keyLabel->setCaption("");
	ctrl.showScreen(HOME);
}

