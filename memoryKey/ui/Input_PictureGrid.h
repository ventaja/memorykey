/**
 * Description: A screen that displays a grid of pictures
 * 				that may each be pressed. This allows a combination
 * 				to be produced for generating a key.
 * @author Adam Knox
 */

#ifndef INPUT_PICTUREGRID_H
#define INPUT_PICTUREGRID_H


// Included Libraries
#include <MAUI/Image.h>
#include <MAUI/Label.h>
#include <MAUI/Layout.h>
#include <MAUI/ListBox.h>
#include <MAUI/Screen.h>
#include <MAUI/Scaler.h>
#include <MAUtil/List.h>
#include <mastdlib.h>


// Included Project Headers
#include "../Controller.h"
#include "../model/Password.h"
#include "../Controller.h"
#include "Theme.h"


//Need to include this if anything is pulled from the resources folder
#include "MAHeaders.h"


// Forward Declarations
class Password;

// Picture Grid Window Class Declaration
class Input_PictureGrid :
	public MAUI::Screen
{
public:
	/**
	 * Constructor: Builds the user interface
	 * @author Adam Knox
	 * @param ui the controller of this object
	 */
	Input_PictureGrid();

	/**
	 * Destructor
	 * @author Adam Knox
	 */
	virtual ~Input_PictureGrid();

	/**
	 * Opens the picture grid input screen using the given password object
	 * @author Adam Knox
	 * @param local_password the params for the key to be generated
	 */
	void open(const Password local_password);

private:

	/**
	 * Runs commands for any key presses on the window's buttons
	 * @author Adam Knox
	 * @param keyCode the code associated with the key pressed
	 */
	void keyPressEvent(const int keyCode);

	/**
	 * Listens for a press on the screen and runs proper action for the press
	 * @author Adam Knox
	 * @param point The position on the screen that is being pressed
	 */
	void pointerPressEvent(const MAPoint2d point);

	/**
	 * Listens for the release of a point on the screen and runs proper action for the release
	 * @author Adam Knox
	 * @param point The position on the screen that is being released
	 */
	void pointerReleaseEvent(const MAPoint2d point);

	// Put the images in a grid format for indexing
	MAUtil::Vector< MAUtil::Vector<MAUI::Image*> > imageGrid;

	// Pointers
	MAUI::Label* 			goButton;
	MAUI::Label* 			secondaryButton;
	MAUI::Label* 			headerLabel; 	// A text label that displays instructions
	MAUI::Layout* 			gridLayout;
	MAUI::Layout*			actionButtonGrid;
	MAUI::Layout* 			mainLayout; 	// The main layout that holds the other widgets
	MAUI::ListBox* 			body;			// area containing the buttons
	Theme* 					theme;

	// Stack Data
	int 					selectedX;
	int 					selectedY;
	Password 				password;
	MAUtil::Vector<int> 	iconSequence;

};

#endif
