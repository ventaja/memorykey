/**
 * The screen used for setting up a new password
 * @author Adam Knox
 */


#include "RenewLicense.h"

/**
 * Constructor: Builds the basic layout, then calls the proper function to build the customized part of the layout.
 * @author Adam Knox
 * @param ui The address to the controller of this screen
 * @param window The type of window to display
 */
RenewLicense::RenewLicense() {
	//Initialise the class to pull info for setting themes from
	theme = new Theme();

	// Build the UI's Layout
	mainLayout = new MAUI::Layout(0, 0, theme->layoutWidth, theme->layoutHeight, NULL, 1, 3);
	mainLayout->setHorizontalAlignment(MAUI::Layout::HA_CENTER);

	// Theme the layout
	if (theme->layoutSkin) {
		mainLayout->setSkin(theme->layoutSkin);
		mainLayout->setDrawBackground(true);
	} else if (theme->layoutBGColor != 0) {
		mainLayout->setBackgroundColor(theme->layoutBGColor);
		mainLayout->setDrawBackground(true);
	} else {
		mainLayout->setDrawBackground(false);
	}
	setMain(mainLayout);


	// Setup the screen's header label
	headerLabel = new MAUI::Label(0, 0,	theme->headerWidth, theme->headerHeight, mainLayout, "New License", theme->headerBGColor, theme->headerFont);
	// Theme the header
	if (theme->headerSkin) {
		headerLabel->setSkin(theme->headerSkin);
		headerLabel->setDrawBackground(true);
	} else if (theme->headerBGColor > 0) {
		headerLabel->setBackgroundColor(theme->headerBGColor);
		headerLabel->setDrawBackground(true);
	} else {
		headerLabel->setDrawBackground(true);
	}
	headerLabel->setHorizontalAlignment(MAUI::Label::HA_CENTER);
	headerLabel->setVerticalAlignment(MAUI::Label::VA_CENTER);


	// Body Spacer
	spacer = new MAUI::Label(0, 0, theme->bodyWidth, theme->bodyPadding, mainLayout);
	spacer->setDrawBackground(false);


	// Setup the body
	body = new MAUI::ListBox(0, 0, theme->bodyWidth, theme->bodyHeight, mainLayout, MAUI::ListBox::LBO_VERTICAL, MAUI::ListBox::LBA_LINEAR, false);
	// Theme the scroll window
	if (theme->bodySkin) {
		body->setSkin(theme->bodySkin);
		body->setDrawBackground(true);
	} else if (theme->bodyBGColor > 0) {
		body->setBackgroundColor(theme->bodyBGColor);
		body->setDrawBackground(true);
	} else {
		body->setDrawBackground(false);
	}


	// Field Container
	licenseFieldContainer = new MAUI::Label(0, 0, theme->ebContainerWidth, theme->ebContainerHeight, body, "", theme->ebContainerBGColor, theme->ebContainerFont);
	// Theme field container
	if (theme->ebContainerSkin) {
		licenseFieldContainer->setSkin(theme->ebContainerSkin);
		licenseFieldContainer->setDrawBackground(true);
	} else if (theme->ebContainerBGColor > 0) {
		licenseFieldContainer->setBackgroundColor(theme->ebContainerBGColor);
		licenseFieldContainer->setDrawBackground(true);
	} else {
		licenseFieldContainer->setDrawBackground(false);
	}


	// Set up field's label
	licenseFieldLabel = new MAUI::Label(theme->ebLabelXOffset, theme->ebLabelYOffset, theme->ebLabelWidth, theme->ebLabelHeight, licenseFieldContainer, "License", theme->ebLabelBGColor, theme->ebLabelFont);
	if (theme->ebLabelSkin) {
		licenseFieldLabel->setSkin(theme->ebLabelSkin);
		licenseFieldLabel->setDrawBackground(true);
	} else if (theme->ebLabelBGColor > 0) {
		licenseFieldLabel->setBackgroundColor(theme->ebLabelBGColor);
		licenseFieldLabel->setDrawBackground(true);
	} else {
		licenseFieldLabel->setDrawBackground(false);
	}


	// Set up the field
	licenseField = new MAUI::EditBox(theme->editBoxXOffset, theme->editBoxYOffset, theme->editBoxWidth, theme->editBoxHeight, licenseFieldContainer, "", theme->editBoxBGColor, theme->editBoxFont, true, false, 60, MAUI::EditBox::IM_QWERTY);
	licenseField->activate();
	licenseField->setSelected(false);
	// Theme edit box
	if (theme->editBoxSkin) {
		licenseField->setSkin(theme->editBoxSkin);
		licenseField->setDrawBackground(true);
	} else if (theme->editBoxBGColor > 0) {
		licenseField->setBackgroundColor(theme->editBoxBGColor);
		licenseField->setDrawBackground(true);
	} else {
		licenseField->setDrawBackground(false);
	}


	// A grid holding the two action buttons
	actionButtonGrid = new MAUI::Layout(0, 0, theme->bodyWidth, theme->buttonHeight, body, 2, 1);


	// Build the cancel button
	cancelButton = new MAUI::Label(0, 0, theme->buttonWidth/2, theme->buttonHeight, actionButtonGrid, "Cancel", theme->button2BGColor, theme->buttonFont);
	cancelButton->setHorizontalAlignment(MAUI::Label::HA_CENTER);
	cancelButton->setVerticalAlignment	(MAUI::Label::VA_CENTER);
	// Theme button
	if (theme->button2Skin) {
		cancelButton->setSkin(theme->button2Skin);
		cancelButton->setDrawBackground(true);
	} else if (theme->button2BGColor > 0) {
		cancelButton->setBackgroundColor(theme->button2BGColor);
		cancelButton->setDrawBackground(true);
	} else {
		cancelButton->setDrawBackground(false);
	}


	// Build the create password button
	applyButton = new MAUI::Label(0, 0, theme->buttonWidth/2, theme->buttonHeight, actionButtonGrid, "Apply", theme->button1BGColor, theme->buttonFont);
	applyButton->setHorizontalAlignment(MAUI::Label::HA_CENTER);
	applyButton->setVerticalAlignment	(MAUI::Label::VA_CENTER);
	// Theme button
	if (theme->button1Skin) {
		applyButton->setSkin(theme->button1Skin);
		applyButton->setDrawBackground(true);
	} else if (theme->button1BGColor > 0) {
		applyButton->setBackgroundColor(theme->button1BGColor);
		applyButton->setDrawBackground(true);
	} else {
		applyButton->setDrawBackground(false);
	}

	// Set the buttons' states
	licenseField->setSelected(true);
	applyButton->setSelected(false);
	cancelButton->setSelected(false);
}





/**
 * Destructor
 * @author Adam Knox
 */
RenewLicense::~RenewLicense() {
	delete headerLabel;
	delete licenseFieldLabel;
	delete actionButtonGrid;
	delete licenseField;
	delete applyButton;
	delete mainLayout;
	delete body;
	delete theme;
	delete cancelButton;
	delete spacer;
	delete licenseFieldContainer;
}




void RenewLicense::open() {
	maShowVirtualKeyboard();
	licenseField->setText("");
	licenseField->setSelected(true);
	applyButton->setSelected(false);
	this->show();
}




/**
 * Listens for any key presses and does the associated action
 * @author Adam Knox
 * @param keyCode The code for the key pressed
 */
void RenewLicense::keyPressEvent(const int keyCode, const int nativeCode) {
	Controller ctrl;

	switch (keyCode) {
		case MAK_BACK:
		case MAK_SOFTLEFT:
			// Hide this screen and go back to the one that called it
			this->hide();
			licenseField->setText("");
			ctrl.showScreen(HOME);
			break;

		case MAK_UP:
			licenseField->setSelected(true);
			maShowVirtualKeyboard();
			applyButton->setSelected(false);
			cancelButton->setSelected(false);
			break;

		case MAK_DOWN:
			if (!cancelButton->isSelected()) {
				licenseField->setSelected(false);
				applyButton->setSelected(true);
			}
			break;

		case MAK_LEFT:
			if (licenseField->isSelected()) {
				licenseField->moveCursorHorizontal(-1,false);
			} else {
				cancelButton->setSelected(true);
				applyButton->setSelected(false);
			}
			break;


		case MAK_RIGHT:
			if (licenseField->isSelected()) {
				licenseField->moveCursorHorizontal(1,false);
			} else {
				cancelButton->setSelected(false);
				applyButton->setSelected(true);
			}
			break;

		case MAK_FIRE:
			if (applyButton->isSelected()) {
				applyLicense();
			} else if (cancelButton->isSelected()) {
				this->hide();
				licenseField->setText("");
				ctrl.showScreen(HOME);
			}
			break;
/*
		case 12:
		case 127:
			// Try to delete the last character in the edit box
			if (selectedEditBox != NULL) {
				MAUI::EditBox* eb = (MAUI::EditBox*)selectedEditBox;
				MAUtil::String newCaption = eb->getCaption();
				if (newCaption.length() > 0) {
					eb->setCaption(newCaption.substr(0, newCaption.length() - 1));
				}
			}
*/
		default:
			// Try to put the character in an editbox
			if (licenseField->isSelected()) {
				MAUtil::String character = Convert::asciiCodeToString(keyCode);
				MAUtil::String newCaption = licenseField->getCaption();
				newCaption.append(character.c_str(), character.length());
				licenseField->setCaption(newCaption);
				licenseField->moveCursorHorizontal(1, false);
			}
			break;
	}
}





/**
 * Listens for a press on the screen and runs proper action for the press
 * @author Adam Knox
 * @param point The position on the screen that is being pressed
 */
void RenewLicense::pointerPressEvent(const MAPoint2d point) {
	pt = point;
}





/**
 * Listens for the release of a point on the screen and runs proper action for the release
 * @author Adam Knox
 * @param point The position on the screen that is being released
 */
void RenewLicense::pointerReleaseEvent(const MAPoint2d point) {
	if ((licenseField->contains(point.x, point.y)) && (licenseField->contains(pt.x, pt.y))) {
		maShowVirtualKeyboard();
		licenseField->setSelected(true);
		applyButton->setSelected(false);
	} else if ((applyButton->contains(point.x, point.y)) && (applyButton->contains(pt.x, pt.y))) {
		applyLicense();

	} else if ((cancelButton->contains(point.x, point.y)) && (cancelButton->contains(pt.x, pt.y))) {
		Controller ctrl;
		this->hide();
		licenseField->setText("");
		ctrl.showScreen(HOME);
	}
}



void RenewLicense::applyLicense() {
	Controller ctrl;
	this->hide();
	ctrl.applyLicense(licenseField->getText().c_str());
	licenseField->setText("");
	ctrl.showScreen(HOME);
}
