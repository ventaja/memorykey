/**
 * Displays to the screen the key created by
 * the algorithm using the password constraints
 * @author Adam Knox
 */


#ifndef VIEW_KEY_H
#define VIEW_KEY_H


// Included Libraries
#include <MAUI/ListBox.h>


//Included Project Headers
#include "../Controller.h"
#include "Theme.h"


//Forward Declarations


// Key_View Class Declaration
class ViewKey :
	public MAUI::Screen
{
public:

	/**
	 * Constructor. Builds the screen for viewing the key
	 * @author Adam Knox
	 * @param ui the object handling this screen
	 */
		ViewKey();

	/**
	 * Destructor. Gets rid of screen and its components
	 * @author Adam Knox
	 */
	virtual ~ViewKey();

	/**
	 * shows the given key on the key display screen
	 * @author Adam Knox
	 * @param local_key the key to display to the user
	 */
	void showKey(MAUtil::String local_key);


private:
	void goHome();

	/**
	 * This method is called when a key is pressed.
	 * @author Adam Knox
	 * @param keyCode the code assignment to the key pressed
	 *
	 */
	void keyPressEvent(const int keyCode);

	/**
	 * Listens for a press on the screen and runs proper action for the press
	 * @author Adam Knox
	 * @param point The position on the screen that is being pressed
	 */
	void pointerPressEvent(const MAPoint2d point);

	/**
	 * Listens for the release of a point on the screen and runs proper action for the release
	 * @author Adam Knox
	 * @param point The position on the screen that is being released
	 */
	void pointerReleaseEvent(const MAPoint2d point);


	// Pointers
	MAUI::Label* 	doneButton;		// Done button widget
	MAUI::Label* 	headerLabel;	// Header label widget
	MAUI::Label* 	keyLabel;		// Key display label widget
	MAUI::Layout* 	mainLayout;		// Layout widget
	MAUI::ListBox*	body;			// Holds the screen's main content
	Theme* 			theme;			// Pointer to theme info

	// Stack Data
	MAUtil::String	 key;			// The key produced from the generator algorithm
};

#endif
